![PIMZ Logo](logo.png)
# PIMZ
**P**arallel **I**mplementation of **M**AME **Z**ip. Modifies zip files so that the resulting output file is always the same, regardless of the operating system, underlying hardware architecture, or file metadata.


## Versions
This version is 0.1.0.

The latest version can be found at [GitLab](https://gitlab.com/awisesoftware/pimz/).

See [documentation/changelog.txt](documentation/changelog.txt) for details.

## Installing

### Binaries
Operating system specific installation packages are available at [GitLab: A.Wise Software - Binaries](https://gitlab.com/awisesoftware/binaries/). Packages exist for macOS, Debian based Linux distributions, Fedora based Linux distributions, and Windows. All systems except macOS have both 32 bit and 64 bit versions available.

### Source
Installation from source is suggested for experienced users only. Details can be found in [documentation/overview.txt](documentation/overview.txt).

## Upgrading

### Binaries
The installer will uninstall the older version.

### Source
Upgrading from source is suggested for experienced users only. Details can be found in [documentation/overview.txt](documentation/overview.txt).

## Uninstalling

### Binaries
Stop the program before uninstalling. Under Windows, if the program is still running during the uninstall process, the executable and installation directory will not be removed.

### Source
Uninstallation from source is suggested for experienced users only. Details can be found in [documentation/overview.txt](documentation/overview.txt).

## Problems
Solutions to common problems can be found in [documentation/problems.txt](documentation/problems.txt).

If you do not find the solution to your problem in the documentation, it may be a bug. Refer to the **Contributing** section on how to view recent issues and/or submit bug reports.

## Contributing
This project is hosted at [GitLab](https://gitlab.com/awisesoftware/pimz/).

Ideas, bugs, etc., can be added to the issue tracker at [GitLab](https://gitlab.com/awisesoftware/pimz/issues) (registration required) or sent to me via [email](mailto:awisesoftware@gmail.com).

## Authors

* **Alan Wise** <awisesoftware@gmail.com> - Initial work.

## License
This file is part of PIMZ.  
Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

See [documentation/license.txt](documentation/license.txt) for details.
