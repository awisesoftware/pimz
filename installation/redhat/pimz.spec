%define MmVersion 0.1

Name:           pimz
Version:        %{MmVersion}.1
Release:        1%{?dist}

Summary:        Parallel Implementation of MAME Zip
License:        GPLv3+
URL:            https://gitlab.com/awisesoftware/%{name}
Source:         https://gitlab.com/awisesoftware/binaries/files/%{MmVersion}/%{name}-%{version}.tar.gz
BuildRequires:  argtable-devel,boost-devel,cmake >= 3.9,gcc-c++,libcurl-devel,minizip-devel,zlib-devel

%description
Parallel Implementation of MAME Zip. Modifies zip files so that the resulting
output file is always the same, regardless of the operating system, underlying
hardware architecture, or file metadata.

%prep
%setup -q
cmake -DCMAKE_INSTALL_PREFIX="/usr" -DCMAKE_BUILD_TYPE="Release" -DPACKAGE_COMPILE_FLAGS="-g"

%build
make %{?_smp_mflags}

%install
%make_install

%files
%{_bindir}/%{name}
%{_datadir}/icons/%{name}.png
%license %{_docdir}/%{name}/license.txt
%{_docdir}/%{name}/changelog.txt
%{_mandir}/man1/%{name}.1.gz

%changelog
* Fri Jan 25 2019 Alan Wise <awisesoftware@gmail.com> - 0.1.1-1
- Build system changes.

* Wed Jul 18 2018 Alan Wise <awisesoftware@gmail.com> - 0.1.0-1
- Initial release.
