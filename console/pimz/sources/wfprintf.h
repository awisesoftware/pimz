/**
*** \file wfprintf.h
*** \brief Width fprintf.
*** \details Provides the same functionality as fprintf(), while limiting line
***   length to the value of the "Width" function parameter. Lines are split at
***   whitespace. Spaces and tabs are replaced with new lines.
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(WFPRINTF_H)
/**
*** \internal
*** \brief wfprintf.h identifier.
*** \details Identifier for wfprintf.h.
**/
#define   WFPRINTF_H


/****
*****
***** INCLUDES
*****
****/

#include  <stdio.h>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if     defined(__cplusplus)
extern "C" {
#endif  /* defined(__cplusplus) */

/**
*** \brief Width fprintf.
*** \details Provides the same functionality as fprintf(), while limiting line
***   length to the value of the "Width" function parameter. Lines are split at
***   whitespace. Spaces and tabs are replaced with new lines.
*** \param pStream Output stream.
*** \param Width Maximum width of a line.
*** \param pFormat Format string.
*** \param ... Additional parameters.
*** \retval >=0 The number of characters printed (excluding the terminator char).
*** \retval -1 Error.
**/
int wfprintf(FILE *pStream,unsigned Width,char const *pFormat,...);

#if     defined(__cplusplus)
}
#endif  /* defined(__cplusplus) */


#endif    /* !defined(WFPRINTF_H) */


/**
*** wfprintf.h
**/
