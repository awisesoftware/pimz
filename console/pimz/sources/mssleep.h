/**
*** \file mssleep.h
*** \brief Temporarily suspend thread execution.
*** \details Suspends execution of the calling thread for a specified amount
***   of time.
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(MSSLEEP_H)
/**
*** \internal
*** \brief mssleep.h identifier.
*** \details Identifier for mssleep.h.
**/
#define   MSSLEEP_H


/****
*****
***** INCLUDES
*****
****/


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if     defined(__cplusplus)
extern "C" {
#endif  /* defined(__cplusplus) */

/**
*** \brief Temporarily suspend thread execution.
*** \details Suspends execution of the calling thread for a specified amount
***   of time.
*** \param Milliseconds Time.
**/
void msSleep(unsigned long Milliseconds);

#if     defined(__cplusplus)
}
#endif  /* defined(__cplusplus) */


#endif    /* !defined(MSSLEEP_H) */


/**
*** mssleep.h
**/
