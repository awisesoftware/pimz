/**
*** \file wfprintf.c
*** \brief wfprintf.c implementation.
*** \details Implementation file for wfprintf.c.
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief wfprintf.c identifier.
*** \details Identifier for wfprintf.c.
**/
#define   WFPRINTF_C


/****
*****
***** INCLUDES
*****
****/

#include  "wfprintf.h"
#if       defined(DEBUG_WFPRINTF_C)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_WFPRINTF_C) */
#include  "awisetoolbox/generic/debuglog.h"
#include  "awisetoolbox/generic/stdio_msvc.h"

#include  <stdlib.h>
#include  <string.h>
#include  <stdarg.h>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

int wfprintf(FILE *pStream,unsigned Width,char const *pFormat,...)
{
  va_list VAList;
  char *pString;
  int Return;
  char *pStartOfLine;
  char *pPrevPtr;
  char *pPtr;
  size_t Length;
  size_t LineLength;


  va_start(VAList,pFormat);
#if       defined(__GNUC__) && !defined(__APPLE__)
#pragma   GCC diagnostic push
#pragma   GCC diagnostic ignored "-Wformat-nonliteral"
#endif    /* defined(__GNUC__) && !defined(__APPLE__) */
  Return=vasprintf(&pString,pFormat,VAList);
#if       defined(__GNUC__) && !defined(__APPLE__)
#pragma   GCC diagnostic pop
#endif    /* defined(__GNUC__) && !defined(__APPLE__) */
  va_end(VAList);

  if (Return!=-1)
  {
    pStartOfLine=pString;
    do
    {
      pPrevPtr=NULL;
      LineLength=0;
      goto wfprintf_Into;

      while(pPtr!=NULL)
      {
        Length=(size_t)(pPtr-pStartOfLine);
        while(Length>Width)
          Length-=Width;
        LineLength+=Length;
        if (LineLength>Width)
          break;
        if (*pPtr==' ')
          LineLength++;
        else if (*pPtr=='\n')
          LineLength=0;
        else if (*pPtr=='\t')
          LineLength=(LineLength+7)&(unsigned)~7;
        pPrevPtr=pPtr;
        pStartOfLine=pPrevPtr+1;
wfprintf_Into:
        pPtr=strpbrk(pStartOfLine," \n\t");
      }

      if (pPtr!=NULL)
      {
        pPtr=pStartOfLine;
        if (pPrevPtr!=NULL)
          *pPrevPtr='\n';
      }
    }
    while(pPtr!=NULL);

    Return=fprintf(pStream,"%s",pString);

    free(pString);
  }

  return(Return);
}


#undef    WFPRINTF_C


/**
*** wfprintf.c
**/
