/**
*** \file pimz.cpp
*** \brief pimz.h implementation file.
*** \details Implementation file for pimz.h.
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief pimz.cpp identifier.
*** \details Identifier for pimz.cpp.
**/
#define   PIMZ_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "pimz.h"
#if       defined(DEBUG_PIMZ_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_PIMZ_CPP) */
#include  "awisetoolbox/generic/debuglog.h"
#include  "awisetoolbox/generic/sysdefs.h"

#include  "config.h"
#include  "licenseagreement.h"
#include  "filelist.h"
#include  "update.h"
#include  "versionstring.h"
#include  "wfprintf.h"

#include  <argtable2.h>
#include  <limits.h>
#include  <thread>

using namespace std;


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/


/**
*** \brief Application starting point.
*** \details Operating system entry function for the application. Initializes
***   the application, reads the command line options, and starts processing
***   data.
*** \param ArgC Number of command line arguments.
*** \param ppArgV Pointer to array of command line argument strings.
*** \retval 0 Success.
*** \retval 1 Error.
*** \retval 2 Internal error.
**/
int main(int ArgC,char **ppArgV)
{
  struct arg_lit *pLogFileArg=
      arg_lit0("l","logfile",
      "Save results to a file                             "
      "(" PIMZ_EXECUTABLENAME "-YYYYMMDD-hh:mm:ss.log)");
  struct arg_str *pOutputNameArg=
      arg_str0("o","output","FILE",
      "Specify logfile pathname (implies '-l')");
  struct arg_int *pProcessesArg=
      arg_int0("p","processes","N",
      "Allow up to N processes                            "
      "(default is the number of available cores)");
  struct arg_lit *pRecurseArg=
      arg_lit0("r","recurse","Process files recursively");
  struct arg_lit *pTestArg=
      arg_lit0("t","test","Test files only, do not make changes");
  struct arg_lit *pVerboseArg=
      arg_lit0("v","verbose","Verbose messages to the terminal");
  struct arg_lit *pHelpArg=
      arg_lit0(NULL,"help","Print help and exit");
  struct arg_lit *pLicenseArg=
      arg_lit0(NULL,"license","Print license and exit");
  struct arg_lit *pUpdateArg=
      arg_lit0(NULL,"update","Check for an update and exit"
#if       defined(PIMZ_NOUPDATECHECKFLAG)
          " (DISABLED)"
#endif    /* defined(PIMZ_NOUPDATECHECKFLAG) */
      );
  struct arg_lit *pVersionArg=
      arg_lit0(NULL,"version","Print version information and exit");
  struct arg_str *pNamesArg=
      arg_strn(NULL,NULL,"DIR/FILE",1,SHRT_MAX,NULL);
  struct arg_end *pEndArg=
      arg_end(20);
  void *ppArgTable[]=
      {
        pLogFileArg,
        pOutputNameArg,
        pProcessesArg,
        pRecurseArg,
        pTestArg,
        pVerboseArg,
        pHelpArg,
        pLicenseArg,
        pUpdateArg,
        pVersionArg,
        pNamesArg,
        pEndArg
      };
  MESSAGEMANAGER_C MessageManager;
  MESSAGEMANAGER_C::MESSAGEBUFFER_C Messages;
  EXITCODE_E ExitCode;
  int ErrorCount;
  OPTIONS_T Options;
  string LogFilePathname;
  char const **ppNames;
  FILELIST_C FileList;

  DEBUGLOG_Initialize(!0);

  DEBUGLOG_Printf2("main(%d,%p)",ArgC,ppArgV);
  DEBUGLOG_LogIn();

  if (arg_nullcheck(ppArgTable)!=0)
  {
    Messages.InternalError(__FILENAME__,__LINE__);
    ExitCode=EXIT_INTERNALERROR;
  }
  else
  {
    ExitCode=EXIT_OK;

    /* Parse the command line. */
    ErrorCount=arg_parse(ArgC,ppArgV,ppArgTable);

    /* Special options (help, license, update, and version) take precedence
        over error reporting. */
    if (pHelpArg->count!=0)
    {
      wfprintf(stdout,80,"%s\n\n",PIMZ_DESCRIPTION);
      fprintf(stdout,"Usage: " PIMZ_EXECUTABLENAME);
      arg_print_syntax(stdout,ppArgTable,"\n");
      arg_print_glossary_gnu(stdout,ppArgTable);
      fprintf(stdout,
          "The exit code is 0 on success, 1 on error, 2 on internal error.\n");
    }
    else if (pVersionArg->count!=0)
    {
      fprintf(stdout,PIMZ_DISPLAYNAME " " PIMZ_VERSION "\n"
          PIMZ_COPYRIGHTNOTICE " <awisesoftware@gmail.com>\n"
          "Web URL: " PIMZ_WEBSITEURL "\n" PIMZ_LICENSE "\n");
    }
    else if (pUpdateArg->count!=0)
    {
#if       !defined(PIMZ_NOUPDATECHECKFLAG)
      try
      {
        string Version;


        CheckForUpdate(
            string(PIMZ_BINARIESURL"/raw/master/PIMZ/current_release"),Version);
        if (VERSIONSTRING_C(PIMZ_VERSION)>=VERSIONSTRING_C(Version.c_str()))
          Messages.Information("No update available.");
        else
          Messages.Information("Version "+Version+" is available!");
      }
      catch(runtime_error const &REException)
      {
        string What;

        What=REException.what();
        Messages.Error(What + ".");
        ExitCode=EXIT_ERROR;
      }
#else     /* !defined(PIMZ_NOUPDATECHECKFLAG) */
        ExitCode=EXIT_ERROR;
#endif    /* !defined(PIMZ_NOUPDATECHECKFLAG) */
    }
    else if (pLicenseArg->count!=0)
    {
      /* Use spaces to match the license formatting... */
      fprintf(stdout,
          "                            " PIMZ_DISPLAYNAME " " PIMZ_VERSION "\n"
          "                   " PIMZ_COPYRIGHTNOTICE "\n");
      fprintf(stdout,"\n%s\n",f_pLicenseAgreement);
    }
    else if (ErrorCount>0)
    {
      /* Parse errors. */
      arg_print_errors(stdout,pEndArg,PIMZ_EXECUTABLENAME);
      fprintf(stdout,
          "Try '" PIMZ_EXECUTABLENAME " --help' for more information.\n");
    }
    else
    {
      /* Defaults. */
      memset(&Options,0,sizeof(Options));

      /* Process options. */
      if (pLogFileArg->count!=0)
      {
        time_t Time;


        LogFilePathname.append(256,'\0');
        Time=time(NULL);
        if (strftime(&LogFilePathname[0],LogFilePathname.capacity()-1,
            PIMZ_EXECUTABLENAME "-%Y%m%d-%H:%M:%S.log",localtime(&Time))==0)
        {
          Messages.InternalError(__FILENAME__,__LINE__);
          ExitCode=EXIT_INTERNALERROR;
        }
        else
        {
          // Trim excess '\0'.
          LogFilePathname.resize(strlen(LogFilePathname.c_str()));
        }
      }
      if (ExitCode==EXIT_OK)
      {
        if (pOutputNameArg->count!=0)
          LogFilePathname=(*pOutputNameArg->sval);
        if (!LogFilePathname.empty())
          MessageManager.SetLogFilePathname(LogFilePathname);
        Options.ProcessCount=thread::hardware_concurrency();
            //  If the value is not well defined or not computable, returns ​0​.
        if (Options.ProcessCount==0)
          Options.ProcessCount=1;
        if (pProcessesArg->count!=0)
        {
          if ((*(pProcessesArg->ival))<=0)
          {
            Messages.Error((string("invalid process count (") +
                to_string(*(pProcessesArg->ival)) + ").").c_str());
            ExitCode=EXIT_ERROR;
          }
          else if ((*(pProcessesArg->ival))>(8*Options.ProcessCount))
          {
            Messages.Error((string(
                "the process count is limited to 8 times the number of cores (") +
                to_string(Options.ProcessCount) + ").").c_str());
            ExitCode=EXIT_ERROR;
          }
          else if ((*(pProcessesArg->ival))>Options.ProcessCount)
          {
            Messages.Warning((string("the process count (") +
                to_string(*(pProcessesArg->ival)) +
                ") exceeds the number of cores (" +
                to_string(Options.ProcessCount) + ").").c_str());
            Options.ProcessCount=*(pProcessesArg->ival);
          }
          else
            Options.ProcessCount=*(pProcessesArg->ival);
        }
        Options.RecurseFlag=(pRecurseArg->count!=0);
        Options.TestFlag=(pTestArg->count!=0);
        if (pVerboseArg->count!=0)
          Options.VerboseFlag=true;
        /* No option errors... */
        if (ExitCode==EXIT_OK)
        {
          /* Show any warnings. */
          MessageManager.TakeMessages(Messages);
          MessageManager.Print(false);
          MessageManager.Save();

          /* Build the list of directory/file names. */
          ppNames=pNamesArg->sval;
          while(strlen(*ppNames)!=0)
          {
            FileList.AddSearchPath(*ppNames);
            ppNames++;
          }

          ExitCode=FileList.Process(Options,MessageManager);
        }
      }
    }

    arg_freetable(ppArgTable,sizeof(ppArgTable)/sizeof(*ppArgTable));
  }

  MessageManager.TakeMessages(Messages);
  MessageManager.Print(false);
  MessageManager.Save();

  DEBUGLOG_LogOut();
  return(ExitCode);
}


#undef    PIMZ_CPP


/**
*** pimz.cpp
**/
