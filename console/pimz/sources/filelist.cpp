/**
*** \file filelist.cpp
*** \brief
*** \details
*** \todo
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief filelist.cpp identifier.
*** \details Identifier for filelist.cpp.
**/
#define   FILELIST_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "filelist.h"
#if       defined(DEBUG_FILELIST_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_FILELIST_CPP) */
#include  "awisetoolbox/generic/debuglog.h"
#include  "awisetoolbox/generic/sysdefs.h"

#include  "mssleep.h"
#include  "zipfile.h"

#include  <atomic>
#include  <boost/filesystem.hpp>
#include  <regex>
#include  <thread>

using namespace boost::filesystem;
using namespace std;


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Thread status.
*** \details Information set by the currently executing threads.
**/
typedef struct structTHREADSTATUS
{
  /**
  *** \brief Finished thread count.
  *** \details Number of threads that have finished.
  **/
  atomic<int> DoneCounter;
  /**
  *** \brief Application exit code.
  *** \details Exit code for the application.
  **/
  atomic<EXITCODE_E> ExitCode;
} THREADSTATUS_T;


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** CONSTANTS
*****
****/

/**
*** \brief Screen/file update delay.
*** \details Delay between screen and/or file updates.
**/
static int const SLEEP_INTERVAL=100;  // in ms.

/**
*** \brief Default 'zip' extension.
*** \details Default extension when searching for 'zip' files.
*** \returns "zip".
**/
static std::string const EXTENSION_ZIP("zip");


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

/**
*** \brief Replace all occurances of a character.
*** \details Replaces all occurances of a character with a string.
*** \param Source String to search/replace.
*** \param Replace Character to replace.
*** \param Replacement String to substitute in place of character.
**/
static void replace_all(string &Source,char Replace,string const &Replacement)
{
  size_t Pos;


  Pos=0;
  while( (Pos<Source.length()) &&
      (string::npos!=(Pos=Source.find(Replace,Pos))) )
  {
    Source.replace(Pos,1,Replacement);
    Pos+=Replacement.length();
  }

  return;
}

/**
*** \brief Process a single pathname from a list of pathnames.
*** \details Iterates through a list of zip files, checking if the CRC in the
***   Torrrent zip comment matches the CRC of the data in the zip file. If the
***   comment doesn't exist or is not correct, converts (possibly, depends on
***   the options) the zip to a Torrent zip.
*** \param pFileList List of pathnames to process.
*** \param Options Processing options.
*** \param MessageManager Message manager to receive messages.
*** \param ThreadStatus Thread status.
**/
static void ProcessFile(FILELIST_C *pFileList,OPTIONS_T const &Options,
    MESSAGEMANAGER_C &MessageManager,THREADSTATUS_T &ThreadStatus)
{
  path Path;
  bool CurrentFlag;
  MESSAGEMANAGER_C::MESSAGEBUFFER_C Messages;


  /* Keep looping until the list is empty. */
  pFileList->Lock();
  while(pFileList->Size()!=0)
  {
    /* Grab the pathname. */
    Path=pFileList->TakeFilename();
    pFileList->Unlock();

    try
    {
      CurrentFlag=ZipFile_IsTorrentZipCommentCurrent(Path.string());
      if (CurrentFlag)
        Messages.Information(Path.filename().string()+": current");
      else
      {
        Messages.Information(Path.filename().string()+": not current");
        MessageManager.TakeMessages(Messages);  // Update immediately.
        if (Options.TestFlag==false)
          ZipFile_ConvertToTorrentZipFile(Path.string(),MessageManager);
      }
    }
    catch(bad_alloc const &BAException)
    {
      Messages.Error(
          string(BAException.what())+" ("+Path.filename().string()+").");
      ThreadStatus.ExitCode=EXIT_INTERNALERROR;
    }
    catch(invalid_argument const &IAException)
    {
      Messages.Error(
          string(IAException.what())+" ("+Path.filename().string()+").");
      ThreadStatus.ExitCode=EXIT_INTERNALERROR;
    }
    catch(runtime_error const &REException)
    {
      Messages.Error(
          string(REException.what())+" ("+Path.filename().string()+").");
      ThreadStatus.ExitCode=EXIT_INTERNALERROR;
    }
    catch(exception const &Exception)
    {
      Messages.Error(
          string(Exception.what())+" ("+Path.filename().string()+").");
      ThreadStatus.ExitCode=EXIT_INTERNALERROR;
    }

    /* Update the console. */
    MessageManager.TakeMessages(Messages);

    pFileList->Lock();
  }
  pFileList->Unlock();

  /* Let the thread monitor know that this thread is done. */
  ThreadStatus.DoneCounter++;

  return;
}

FILELIST_C::FILELIST_C(void)
{
  DEBUGLOG_Printf0("FILELIST_C::FILELIST_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

FILELIST_C::~FILELIST_C(void)
{
  DEBUGLOG_Printf0("FILELIST_C::~FILELIST_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

void FILELIST_C::AddSearchPath(string const &Path)
{
  DEBUGLOG_Printf2("FILELIST_C::AddSearchPath(%p(%s))",&Path,Path.c_str());
  DEBUGLOG_LogIn();

  /* Only add pathname if unique. */
  m_SearchPath.insert(Path);

  DEBUGLOG_LogOut();
  return;
}

EXITCODE_E FILELIST_C::HandleDirectory(VALUE_C const &Pathname)
{
  DEBUGLOG_Printf2(
      "FILELIST_C::HandleDirectory(%p(%s))",&Pathname,Pathname.c_str());
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return(HandleWildcard(Pathname/("*."+EXTENSION_ZIP)));
}

EXITCODE_E FILELIST_C::HandleFile(VALUE_C const &Pathname)
{
  DEBUGLOG_Printf2(
      "FILELIST_C::HandleFile(%p(%s))",&Pathname,Pathname.c_str());
  DEBUGLOG_LogIn();

  m_FoundPathnames.insert(Pathname);

  DEBUGLOG_LogOut();
  return(EXIT_OK);
}

EXITCODE_E FILELIST_C::HandleOther(VALUE_C const &Pathname)
{
  DEBUGLOG_Printf2(
      "FILELIST_C::HandleOther(%p(%s))",&Pathname,Pathname.c_str());
  DEBUGLOG_LogIn();

  // Do nothing.
  UNUSED(Pathname);

  DEBUGLOG_LogOut();
  return(EXIT_OK);
}

EXITCODE_E FILELIST_C::HandleSymbolicLink(VALUE_C const &Pathname)
{
  DEBUGLOG_Printf2(
      "FILELIST_C::HandleSymbolicLink(%p(%s))",&Pathname,Pathname.c_str());
  DEBUGLOG_LogIn();

  // Do nothing.
  UNUSED(Pathname);

  DEBUGLOG_LogOut();
  return(EXIT_OK);
}

EXITCODE_E FILELIST_C::HandleWildcard(VALUE_C const &Pathname)
{
  EXITCODE_E ExitCode;
  MESSAGEMANAGER_C::MESSAGEBUFFER_C Messages;
  string Expression;
  regex_constants::syntax_option_type CaseCompare=regex_constants::grep;
#if       defined(_WIN32) || defined(_WIN64)
  CaseCompare|=regex_constants::icase;
#endif    /* defined(_WIN32) || defined(_WIN64) */


  DEBUGLOG_Printf2(
      "FILELIST_C::HandleWildcard(%p(%s)",&Pathname,Pathname.c_str());
  DEBUGLOG_LogIn();

  ExitCode=EXIT_OK;
  for(directory_iterator pIt(Pathname.parent_path());
      pIt!=directory_iterator();++pIt)
  {
    try
    {
      // TODO Check recursive, add directory.

      /* Skip if not a file. */
      if (is_regular_file(pIt->status()))
      {
        /* Not substring. */
        Expression="^"+Pathname.filename().string();

        /* Convert shell wildcards to regex expressions. */
        replace_all(Expression, '*', ".*") ;
        replace_all(Expression, '?', "(.{1})") ;

        /* Match? */
        if (regex_match(
            pIt->path().filename().string(),regex(Expression,CaseCompare)))
          m_FoundPathnames.insert(pIt->path());
      }
    }
    catch(regex_error const &REException)
    {
      UNUSED(REException);
      Messages.InternalError(__FILENAME__,__LINE__);
      ExitCode=EXIT_INTERNALERROR;
    }
  }

  DEBUGLOG_LogOut();
  return(ExitCode);
}

void FILELIST_C::Lock(void)
{
  DEBUGLOG_Printf0("FILELIST_C::Lock()");
  DEBUGLOG_LogIn();

  m_Mutex.lock();

  DEBUGLOG_LogOut();
  return;
}

FILELIST_C::SIZE_T FILELIST_C::Size(void)
{
  SIZE_T Size;


  DEBUGLOG_Printf0("FILELIST_C::Size()");
  DEBUGLOG_LogIn();

  Size=m_FoundPathnames.size();

  DEBUGLOG_LogOut();
  return(Size);
}

string FILELIST_C::TakeFilename(void)
{
  VALUE_C Value;


  DEBUGLOG_Printf0("FILELIST_C::TakeFilename()");
  DEBUGLOG_LogIn();

  Value=*m_FoundPathnames.begin();
  m_FoundPathnames.erase(Value);

  DEBUGLOG_LogOut();
  return(Value.string());
}

void FILELIST_C::Unlock(void)
{
  DEBUGLOG_Printf0("FILELIST_C::Unlock()");
  DEBUGLOG_LogIn();

  m_Mutex.unlock();

  DEBUGLOG_LogOut();
  return;
}

EXITCODE_E FILELIST_C::Process(
    OPTIONS_T const &Options,MESSAGEMANAGER_C &MessageManager)
{
  THREADSTATUS_T ThreadStatus;
  EXITCODE_E ExitCode;
  EXITCODE_E TempExitCode;


  DEBUGLOG_Printf2("FILELIST_C::Process(%p,%p)",&Options,&MessageManager);
  DEBUGLOG_LogIn();

  /* Convert everything on the command line to a list of actual pathnames. */
  ExitCode=EXIT_OK;
  while(m_SearchPath.size()!=0)
  {
    VALUE_C Pathname;


    Pathname=*m_SearchPath.begin();
    m_SearchPath.erase(Pathname);
    if (is_symlink(Pathname))
      TempExitCode=HandleSymbolicLink(Pathname);
    else if (is_directory(Pathname))
      TempExitCode=HandleDirectory(Pathname);
    else if (is_regular_file(Pathname))
      TempExitCode=HandleFile(Pathname);
    else if (is_other(Pathname))
      TempExitCode=HandleOther(Pathname);
    else
      TempExitCode=HandleWildcard(Pathname);
    if (TempExitCode>ExitCode)
        ExitCode=TempExitCode;
  }

  if (ExitCode!=EXIT_INTERNALERROR)
  {
    vector<thread> Threads;


    /* Initialize thread status. */
    ThreadStatus.ExitCode=ExitCode;
    ThreadStatus.DoneCounter=0;

    /* Create/start the threads. */
    for(int Process=0;Process<Options.ProcessCount;Process++)
      Threads.push_back(thread(ProcessFile,this,
          std::ref(Options),std::ref(MessageManager),std::ref(ThreadStatus)));

    /* Now, while waiting for all of the threads to finish,
        periodically update the screen. */
    do
    {
      unsigned SleepInterval=SLEEP_INTERVAL;


#if       defined(DEBUG) && DEBUG
      SleepInterval*=10;
#endif    /* defined(DEBUG) && DEBUG */

      msSleep(SleepInterval);
      MessageManager.Print(Options.VerboseFlag);
    }
    while(ThreadStatus.DoneCounter!=Options.ProcessCount);

    for(int Process=0;Process<Options.ProcessCount;Process++)
      Threads[Process].join();

    MessageManager.Print(Options.VerboseFlag);


    if (ThreadStatus.ExitCode>ExitCode)
      ExitCode=ThreadStatus.ExitCode;
  }

  DEBUGLOG_LogOut();
  return(ExitCode);
}


#undef    FILELIST_CPP


/**
*** filelist.cpp
**/
