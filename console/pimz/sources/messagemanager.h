/**
*** \file messagemanager.h
*** \brief Manage/display/save application messages.
*** \details Manages application messages, and provides options to display
***   and/or save them.
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(MESSAGEMANAGER_H)
/**
*** \internal
*** \brief messagemanager.h identifier.
*** \details Identifier for messagemanager.h.
**/
#define   MESSAGEMANAGER_H


/****
*****
***** INCLUDES
*****
****/

#include  <list>
#include  <mutex>
#include  <string>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Manage messages.
*** \details Buffers messages from various sources (MESSAGEBUFFER_C), formats
***   the message, and then prints and/or saves the messages as required by the
***   application.
**/
class MESSAGEMANAGER_C
{
  private:
    /**
    *** \todo
    **/
    typedef enum enumMESSAGETYPE
    {
      /**
      *** \brief Error message.
      *** \details Message is an error.
      **/
      MESSAGETYPE_ERROR=0,
      /**
      *** \brief Information message.
      *** \details Message is information.
      **/
      MESSAGETYPE_INFORMATION=1,
      /**
      *** \brief Internal error message.
      *** \details Message is an internal error.
      **/
      MESSAGETYPE_INTERNALERROR=2,
      /**
      *** \brief Verbose information message.
      *** \details Message is verbose information.
      **/
      MESSAGETYPE_VERBOSEINFORMATION=3,
      /**
      *** \brief Warning message.
      *** \details Message is a warning.
      **/
      MESSAGETYPE_WARNING=4
    } MESSAGETYPE_E;
    /**
    *** \brief Output options.
    *** \details Where to print the messages.
    **/
    typedef enum enumOUTPUT
    {
      OUTPUT_TERMINAL=1,
      OUTPUT_FILE=2,
      OUTPUT_ALL=OUTPUT_TERMINAL|OUTPUT_FILE
    } OUTPUT_F;

    /**
    *** \todo
    **/
    class MESSAGELIST_C
    {
      private:
        /**
        *** \brief Message list storage type.
        *** \details Data type of objects stored in the message list.
        **/
        using VALUE_C=std::tuple<MESSAGETYPE_E,std::string,OUTPUT_F>;
        /**
        *** \brief Message list type.
        *** \details Data type of of the message list.
        **/
        using TYPE_C=std::list<VALUE_C>;

      public:
        /**
        *** \brief Iterator.
        *** \details Message list iterator.
        **/
        typedef TYPE_C::iterator ITERATOR_C;
        /**
        *** \brief Constant tterator.
        *** \details Message list constant iterator.
        **/
        typedef TYPE_C::const_iterator CONSTITERATOR_C;

      public:
        /**
        *** \brief Default constructor.
        *** \details Default constructor.
        **/
        MESSAGELIST_C(void);
        /**
        *** \brief Destructor.
        *** \details Destructor.
        **/
        ~MESSAGELIST_C(void);
        /**
        *** \brief Append message list.
        *** \details Appends a message list to another message list.
        *** \param List - Message list to append.
        **/
        void Append(MESSAGELIST_C &List);
        /**
        *** \todo
        *** \returns
        **/
        ITERATOR_C Begin(void);
        /**
        *** \todo
        *** \param Flags
        **/
        void Clear(OUTPUT_F Flags);
        /**
        *** \todo
        *** \returns
        **/
        ITERATOR_C End(void);
        /**
        *** \todo
        *** \param Pos
        *** \param First
        *** \param Last
        **/
        void Insert(CONSTITERATOR_C Pos,ITERATOR_C First,ITERATOR_C Last);
        /**
        *** \todo
        **/
        void Lock(void);
        /**
        *** \todo
        *** \param Type
        *** \param Message
        **/
        void PushBack(MESSAGETYPE_E Type,std::string const &Message);
        /**
        *** \todo
        **/
        void Unlock(void);

      private:
        /**
        *** \todo
        **/
        TYPE_C m_Messages;
        /**
        *** \todo
        **/
        std::mutex m_Mutex;
    };

  public:
    /**
    *** \todo
    **/
    class MESSAGEBUFFER_C
    {
      /**
      *** \todo
      **/
      friend class MESSAGEMANAGER_C;

      public:
        /**
        *** \brief Default constructor.
        *** \details Default constructor.
        **/
        MESSAGEBUFFER_C(void);
        /**
        *** \brief Destructor.
        *** \details Destructor.
        **/
        ~MESSAGEBUFFER_C(void);
        /**
        *** \brief Save error message.
        *** \details Saves an error message to the message list.
        *** \param Message Message.
        **/
        void Error(std::string const &Message);
        /**
        *** \brief Save information message.
        *** \details Saves an information message to the message list.
        *** \param Message Message.
        **/
        void Information(std::string const &Message);
        /**
        *** \brief Save internal error message.
        *** \details Saves an internal error message to the message list.
        *** \param Filename Source file name.
        *** \param LineNumber Source line number.
        **/
        void InternalError(std::string const &Filename,int LineNumber);
        /**
        *** \brief Save verbose information message.
        *** \details Saves a verbose information message to the message list.
        *** \param Message Message.
        **/
        void VerboseInformation(std::string const &Message);
        /**
        *** \brief Save warning message.
        *** \details Saves an warning message to the message list.
        *** \param Message Message.
        **/
        void Warning(std::string const &Message);

      private:
        /** \todo
        *** \param Type
        *** \param Message Message.
        **/
        void SaveMessage(MESSAGETYPE_E Type,std::string const &Message);

      private:
        /**
        *** \todo
        **/
        MESSAGELIST_C m_Messages;
    };

  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    MESSAGEMANAGER_C(void);
    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~MESSAGEMANAGER_C(void);
    /**
    *** \brief Print messages.
    *** \details Prints messages to the console.
    *** \param VerboseFlag Verbose messages flag
    ***   (false=normal messages, true=verbose messages).
    **/
    void Print(bool VerboseFlag);
    /**
    *** \brief Save messages.
    *** \details Savea messages to a log file (only if the log pathname is set).
    **/
    void Save(void);
    /**
    *** \brief Transfer messages.
    *** \details Transfers messages from a message buffer to the message
    ***   manager.
    *** \param Messages Message list to transfer.
    **/
    void TakeMessages(MESSAGEBUFFER_C &Messages);
    /**
    *** \brief Set the log file pathname.
    *** \details Set the pathname of the log file.
    *** \param Pathname Log file pathname.
    **/
    void SetLogFilePathname(std::string const &Pathname);

  private:
    /**
    *** \brief Message list.
    *** \details List of messages.
    **/
    MESSAGELIST_C m_Messages;
    /**
    *** \brief Log file pathname.
    *** \details Pathname of log file.
    **/
    std::string m_LogFilePathname;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if     defined(__cplusplus)
extern "C" {
#endif  /* defined(__cplusplus) */

#if     defined(__cplusplus)
}
#endif  /* defined(__cplusplus) */


#endif    /* !defined(MESSAGEMANAGER_H) */


/**
*** messagemanager.h
**/
