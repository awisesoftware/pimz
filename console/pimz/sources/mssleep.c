/**
*** \file mssleep.c
*** \brief mssleep.c implementation.
*** \details Implementation file for mssleep.c.
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief mssleep.c identifier.
*** \details Identifier for mssleep.c.
**/
#define   MSSLEEP_C


/****
*****
***** INCLUDES
*****
****/

#include  "mssleep.h"
#if       defined(DEBUG_MSSLEEP_C)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_MSSLEEP_C) */
#include  "awisetoolbox/generic/debuglog.h"

#include  <stdlib.h>
    // _POSIX_C_SOURCE is not defined until a header is included first.

#if       defined(__linux__)
#if       _POSIX_C_SOURCE>=199309L
#include  <time.h>    // for nanosleep
#else     /* _POSIX_C_SOURCE < 199309L */
#include  <unistd.h>  // for usleep
#endif    /* _POSIX_C_SOURCE < 199309L */
#elif     defined(WIN32) || defined(_WIN64)
#include  <windows.h>
#endif    /* defined(__linux__), defined(WIN32) || defined(_WIN64) */


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

void msSleep(unsigned long Milliseconds)
{
#if       defined(__linux__)
#if       _POSIX_C_SOURCE>=199309L
  struct timespec Interval;


  Interval.tv_sec=(long)Milliseconds/1000;
  Interval.tv_nsec=1000000*((long)Milliseconds%1000);
  nanosleep(&Interval,NULL);
#else     /* _POSIX_C_SOURCE < 199309L */
  usleep(1000*Milliseconds);
#endif    /* _POSIX_C_SOURCE < 199309L */
#elif     defined(WIN32) || defined(_WIN64)
  Sleep(Milliseconds);
#endif    /* defined(__linux__), defined(WIN32) || defined(_WIN64) */
}


#undef    MSSLEEP_C


/**
*** mssleep.c
**/
