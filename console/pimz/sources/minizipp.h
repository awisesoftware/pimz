/**
*** \file minizipp.h
*** \brief MiniZip C++ classes.
*** \details C++ classes encapsulating the functionality of the MiniZip library.
*** \note Make into a project of its own? Submit to MiniZip project?
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(MINIZIPP_H)
/**
*** \internal
*** \brief minizipp.h identifier.
*** \details Identifier for minizipp.h.
**/
#define   MINIZIPP_H


/****
*****
***** INCLUDES
*****
****/

#include  <string>
#if       MINIZIP_VERSION_MAJOR==1
#include  <unzip.h>
#include  <zip.h>
#elif     MINIZIP_VERSION_MAJOR==2
#include  <mz.h>
#include  <mz_compat.h>
#include  <zlib.h>
#endif    /* MINIZIP_VERSION_MAJOR==1, MINIZIP_VERSION_MAJOR==2 */


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

namespace MiniZip
{
  /**
  *** \brief Extract files from zip file.
  *** \details Extract files from a zip file.
  **/
  class UNZIPFILE_C
  {
    public:
      /**
      *** \hideinitializer
      *** \brief String comparison case sensitivity.
      *** \details Case sensitivity for string comparisons.
      **/
      typedef enum enumCASE
      {
        /**
        *** \brief OS dependent.
        *** \details Sensitivity depends on the operating system.
        ***   (Windows is not case sensitive, Unix/Linux is case sensitive).
        **/
        CASE_OSDEFAULT=0,
        /**
        *** \brief Case sensitive.
        *** \details String comparisions are case sensitive.
        **/
        CASE_SENSITIVE=1,
        /**
        *** \brief Case insensitive.
        *** \details String comparisions are not case sensitive.
        **/
        CASE_INSENSITIVE=2
      } CASE_E;
      /**
      *** \hideinitializer
      *** \brief Function return values.
      *** \details Possible return values from function calls.
      **/
      typedef enum enumRETURN
      {
        /**
        *** \brief No errors.
        *** \details Function returned with no errors.
        **/
        RETURN_OK=UNZ_OK,
        /**
        *** \brief End of file list.
        *** \details A function has reached the last file in the zip file.
        **/
        RETURN_END_OF_LIST_OF_FILE=UNZ_END_OF_LIST_OF_FILE,
        /**
        *** \brief Errno.
        *** \details Undocumented (zlib?).
        **/
        RETURN_ERRNO=UNZ_ERRNO,
        /**
        *** \brief End of file.
        *** \details Function has reached the end of the file.
        **/
        RETURN_EOF=UNZ_EOF,
        /**
        *** \brief Parameter error.
        *** \details Function was passed an invalid parameter.
        **/
        RETURN_PARAMERROR=UNZ_PARAMERROR,
        /**
        *** \brief Bad zip file.
        *** \details Undocumented (zlib?). Corrupt zip file?
        **/
        RETURN_BADZIPFILE=UNZ_BADZIPFILE,
        /**
        *** \brief Internal error.
        *** \details The Minizip library experienced an internal error.
        **/
        RETURN_INTERNALERROR=UNZ_INTERNALERROR,
        /**
        *** \brief CRC error.
        *** \details Undocumented (zlib?). Corrupt zip file?
        **/
        RETURN_CRCERROR=UNZ_CRCERROR
      } RETURN_E;

    public:
      /**
      *** \brief Constructor.
      *** \details Default constructor.
      **/
      UNZIPFILE_C(void);
      /**
      *** \brief Destructor.
      *** \details Default destructor.
      **/
      ~UNZIPFILE_C(void);
      /**
      *** \brief Close zip file.
      *** \details Closes a zip file that was opened with Open().
      *** \returns Unzip error code.
      **/
      RETURN_E Close(void);
      /**
      *** \brief Close current file in an zip file.
      *** \details Closes the file in the zip file that was opened with
      ***   OpenCurrentFile().
      *** \returns Unzip error code.
      **/
      RETURN_E CloseCurrentFile(void);
      /**
      *** \brief Get current file information.
      *** \details Returns information about the current file in the zip file.
      *** \param pFileInfo File information about the current file.
      *** \param pPathname Pointer to storage for the current file pathname.
      *** \param pComment Pointer to storage for the current file comment.
      *** \param pExtraField Pointer to storage for current file extra field data.
      *** \param ExtraFieldSize Size of storage for current file extra field data.
      *** \returns Unzip error code.
      **/
      RETURN_E GetCurrentFileInfo(unz_file_info *pFileInfo,
          std::string *pPathname=NULL,std::string *pComment=NULL,
          void *pExtraField=NULL,uint16_t ExtraFieldSize=0);
      /**
      *** \brief Get global comment.
      *** \details Returns the global comment string of the zip file.
      *** \param Comment Global comment.
      *** \returns Unzip error code.
      **/
      RETURN_E GetGlobalComment(std::string &Comment);
      /**
      *** \brief Get global information.
      *** \details Returns global information about the zip file.
      *** \param pGlobalInfo Pointer to storage for global information.
      *** \returns Unzip error code.
      **/
      RETURN_E GetGlobalInfo(unz_global_info *pGlobalInfo);
      /**
      *** \brief Set first file as current.
      *** \details Sets the first file in the zip file as the current file.
      *** \returns Unzip error code.
      **/
      RETURN_E GoToFirstFile(void);
      /**
      *** \brief Set the next file as current.
      *** \details Sets the next file in the zip file as the current file.
      *** \returns Unzip error code.
      **/
      RETURN_E GoToNextFile(void);
      /**
      *** \brief Locate file within the zip file.
      *** \details Try to locate a file within the zip file.
      *** \param Pathname Pathname of file to locate.
      *** \param CaseSensitivity Case sensitivity of string comparisions.
      *** \returns Unzip error code.
      **/
      RETURN_E LocateFile(std::string const &Pathname,CASE_E CaseSensitivity);
      /**
      *** \brief Open zip file.
      *** \details Opens a zip file for reading.
      *** \param Pathname Pathname of file to open.
      *** \returns Unzip error code.
      **/
      RETURN_E Open(std::string const &Pathname);
      /**
      *** \brief Open current file.
      *** \details Opens the current file in the zip file for reading.
      *** \returns Unzip error code.
      **/
      RETURN_E OpenCurrentFile(void);
      /**
      *** \brief Read current file data.
      *** \details Reads data from the current file in the zip file.
      *** \param pBuffer Pointer to storage for the current file data.
      *** \param Length Size of the storage for the current file data.
      *** \returns Unzip error code.
      **/
      RETURN_E ReadCurrentFile(void *pBuffer,unsigned Length);

    protected:
      /**
      *** \brief Zip file data.
      *** \details Data for the currently open zip file.
      **/
      unzFile m_File;
  };

  /**
  *** \brief Create/append to a zip file.
  *** \details Create a zip file or append files into an existing zip file.
  **/
  class ZIPFILE_C
  {
    public:
      /**
      *** \brief How to apply new file data.
      *** \details How to apply new file data to a new or existing zip file.
      **/
      typedef enum enumAPPEND
      {
        /**
        *** \brief Create.
        *** \details Create new zip file (overwrite existing).
        **/
        APPEND_CREATE=APPEND_STATUS_CREATE,
        /**
        *** \brief Create or append.
        *** \details Creates the file if it doesn't exist, otherwise appends data.
        **/
        APPEND_CREATEAFTER=APPEND_STATUS_CREATEAFTER,
        /**
        *** \brief Append.
        *** \details Appends data to the zip file.
        **/
        APPEND_ADDINZIP=APPEND_STATUS_ADDINZIP
      } APPEND_E;
      /**
      *** \brief Function return values.
      *** \details Possible return values from function calls.
      **/
      typedef enum enumRETURN
      {
        /**
        *** \brief No errors.
        *** \details Function returned with no errors.
        **/
        RETURN_OK=ZIP_OK,
        /**
        *** \brief End of file.
        *** \details Function has reached the end of the file.
        **/
        RETURN_EOF=ZIP_EOF,
        /**
        *** \brief Errno.
        *** \details Undocumented (zlib?).
        **/
        RETURN_ERRNO=ZIP_ERRNO,
        /**
        *** \brief Parameter error.
        *** \details Function was passed an invalid parameter.
        **/
        RETURN_PARAMERROR=ZIP_PARAMERROR,
        /**
        *** \brief Bad zip file.
        *** \details Undocumented (zlib?). Corrupt zip file?
        **/
        RETURN_BADZIPFILE=ZIP_BADZIPFILE,
        /**
        *** \brief Internal error.
        *** \details The Minizip library experienced an internal error.
        **/
        RETURN_INTERNALERROR=ZIP_INTERNALERROR
      } RETURN_E;
      /**
      *** \brief Compression levels.
      *** \details How much effort to expend to compress a file in the zip file.
      **/
      typedef enum enumLEVEL
      {
        /**
        *** \brief Default compression level.
        *** \details Default compression level.
        **/
        LEVEL_DEFAULT=Z_DEFAULT_COMPRESSION,
        /**
        *** \brief Compression level 0.
        *** \details Compression level 0 (no compression).
        **/
        LEVEL_0=Z_NO_COMPRESSION,
        LEVEL_NO_COMPRESSION=Z_NO_COMPRESSION,
        /**
        *** \brief Compression level 1.
        *** \details Compression level 1 (minimal compression).
        **/
        LEVEL_1=Z_BEST_SPEED,
        LEVEL_BEST_SPEED=Z_BEST_SPEED,
        /**
        *** \brief Compression level 2.
        *** \details Compression level 2.
        **/
        LEVEL_2=2,
        /**
        *** \brief Compression level 3.
        *** \details Compression level 3.
        **/
        LEVEL_3=3,
        /**
        *** \brief Compression level 4.
        *** \details Compression level 4.
        **/
        LEVEL_4=4,
        /**
        *** \brief Compression level 5.
        *** \details Compression level 5.
        **/
        LEVEL_5=5,
        /**
        *** \brief Compression level 6.
        *** \details Compression level 6.
        **/
        LEVEL_6=6,
        /**
        *** \brief Compression level 7.
        *** \details Compression level 7.
        **/
        LEVEL_7=7,
        /**
        *** \brief Compression level 8.
        *** \details Compression level 8.
        **/
        LEVEL_8=8,
        /**
        *** \brief Compression level 9.
        *** \details Compression level 9 (maximum).
        **/
        LEVEL_9=Z_BEST_COMPRESSION,
        LEVEL_BEST_COMPRESSION=Z_BEST_COMPRESSION
      } LEVEL_E;
      /**
      *** \brief Compression methods.
      *** \details Methods used to compress data in the zip file.
      **/
      typedef enum enumMETHOD
      {
        /**
        *** \brief Compression method.
        *** \details Method used to compress the files.
        **/
        METHOD_DEFLATE=Z_DEFLATED
      } METHOD_E;

    public:
      /**
      *** \brief Constructor.
      *** \details Default constructor.
      **/
      ZIPFILE_C(void);
      /**
      *** \brief Destructor.
      *** \details Default destructor.
      **/
      ~ZIPFILE_C(void);
      /**
      *** \brief Close zip file.
      *** \details Closes a zip file that was opened with Open().
      *** \param GlobalComment Global zip file comment.
      *** \returns Zip error code.
      **/
      RETURN_E Close(std::string const &GlobalComment="");
      /**
      *** \brief Close current file.
      *** \details Closes the current file in the zip file.
      *** \returns Zip error code.
      **/
      RETURN_E CloseFileInZip(void);
      /**
      *** \brief Open zip file.
      *** \details Opens a zip file for writing.
      *** \param Pathname Pathname of file to open.
      *** \param Append How to apply new file data.
      *** \returns Zip error code.
      **/
      RETURN_E Open(std::string const &Pathname,APPEND_E Append);
      /**
      *** \brief
      *** \details
      *** \param Pathname Pathname of the new file.
      *** \param pZipFileInfo File information of the new file.
      *** \param Comment Comment of the new file.
      *** \param pLocalExtraField Pointer to local extra field data of the
      ***   new file.
      *** \param LocalExtraFieldSize Size of local extra field data of the
      ***   new file.
      *** \param pGlobalExtraField Pointer to local extra field data of the
      ***   new file.
      *** \param GlobalExtraFieldSize Size of local extra field data of the
      ***   new file.
      *** \param Method Compression method.
      *** \param Level Compression level.
      *** \returns Zip error code.
      **/
      RETURN_E OpenNewFileInZip(std::string const &Pathname,
          zip_fileinfo const *pZipFileInfo,std::string const &Comment="",
          void const *pLocalExtraField=NULL,uint16_t LocalExtraFieldSize=0,
          void const *pGlobalExtraField=NULL,uint16_t GlobalExtraFieldSize=0,
          METHOD_E Method=METHOD_DEFLATE,LEVEL_E Level=LEVEL_DEFAULT);
      /**
      *** \brief Copy data into the new file.
      *** \details Copies data into the new file in the zip file.
      *** \param pBuffer Pointer to the file data of the new file.
      *** \param Length Size of the file data of the new file.
      *** \returns Zip error code.
      **/
      RETURN_E WriteInFileInZip(void const *pBuffer,unsigned Length);

    protected:
      /**
      *** \brief Zip file data.
      *** \details Data for the currently open zip file.
      **/
      zipFile m_File;
  };
}


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if     defined(__cplusplus)
extern "C" {
#endif  /* defined(__cplusplus) */

#if     defined(__cplusplus)
}
#endif  /* defined(__cplusplus) */


#endif    /* !defined(MINIZIPP_H) */


/**
*** minizipp.h
**/
