/**
*** \file update.cpp
*** \brief update.h implementation file.
*** \details Implementation file for update.h.
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief update.cpp identifier.
*** \details Identifier for update.cpp.
**/
#define   UPDATE_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "update.h"
#if       defined(DEBUG_UPDATE_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_UPDATE_CPP) */
#include  "awisetoolbox/generic/debuglog.h"
#include  "awisetoolbox/generic/messagelog.h"
#include  "awisetoolbox/generic/sysdefs.h"

#include  <curl/curl.h>
#include  <stdexcept>
#include  <string>

using namespace std;


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

static size_t cURLCallback(
    char const *pData,size_t Size,size_t SizeCount,void *pUserData)
{
  size_t Return;
  std::string *pVersion;


  DEBUGLOG_Printf4("cURLCallback(%p,%u,%u,*p)",pData,Size,SizeCount,pUserData);
  DEBUGLOG_LogIn();

  /* Parameter checking. */
  if (pUserData==NULL)
  {
    MESSAGELOG_LogError(ERRORCODE_NULLPARAMETER);
    Return=0;
  }
  else
  {
    /* Copy and convert the user pointer. */
    pVersion=(std::string*)pUserData;

    /* Copy the data. */
    Return=Size*SizeCount;
    pVersion->append(pData,Return);

    /* Erase any trailing whitespace. */
    pVersion->erase(pVersion->find_last_not_of("\f\n\r\t\v ")+1);
  }

  DEBUGLOG_LogOut();
  return(Return);
}

void CheckForUpdate(string const &URL,string &Version)
{
  CURL *pHandle;
  CURLcode Result;


  DEBUGLOG_Printf2("CheckForUpdate(%p(%s))",&URL,URL.c_str());
  DEBUGLOG_LogIn();

  pHandle=curl_easy_init();
  curl_easy_setopt(pHandle,CURLOPT_URL,URL.c_str());
  curl_easy_setopt(pHandle,CURLOPT_FOLLOWLOCATION,1L);
  curl_easy_setopt(pHandle,CURLOPT_MAXREDIRS,50L);
  curl_easy_setopt(pHandle,CURLOPT_WRITEFUNCTION,cURLCallback);
  curl_easy_setopt(pHandle,CURLOPT_WRITEDATA,&Version);
  curl_easy_setopt(pHandle,CURLOPT_FAILONERROR,true);
  Result=curl_easy_perform(pHandle);
  if (Result!=CURLE_OK)
    throw(runtime_error(string(curl_easy_strerror(Result))));
  curl_easy_cleanup(pHandle);

  DEBUGLOG_LogOut();
  return;
}


#undef    UPDATE_CPP


/**
*** update.cpp
**/
