/**
*** \file pimz.h
*** \brief Application entry point.
*** \details Operating system entry function for the application. Initializes
***   the application, reads the command line options, and starts processing
***   data.
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(PIMZ_H)
/**
*** \internal
*** \brief pimz.h identifier.
*** \details Identifier for pimz.h.
**/
#define   PIMZ_H


/****
*****
***** INCLUDES
*****
****/


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/**
*** \brief Program exit codes.
*** \details Exit codes returned to the operating system.
*** \warning These are in order of severity (least to most) and some code
***   depends on that order. DO NOT REORDER!
**/
typedef enum enumEXITCODE
{
  /**
  *** \brief OK exit code.
  *** \details Informs the operating system that no errors occurred.
  **/
  EXIT_OK=0,
  /**
  *** \brief Error exit code.
  *** \details Informs the operating system that an error occurred.
  **/
  EXIT_ERROR=1,
  /**
  *** \brief Internal error exit code.
  *** \details Informs the operating system that an internal error occurred.
  **/
  EXIT_INTERNALERROR=2
} EXITCODE_E;

/**
*** \brief Command line options.
*** \details Options read from the command line.
**/
typedef struct structOPTIONS
{
  /**
  *** \brief Decend to sub-directories flag.
  *** \details Flag to decend into sub-directories.
  **/
  unsigned int RecurseFlag:1;
  /**
  *** \brief Test zip files flag.
  *** \details Flag to test the zip files (no modifications).
  **/
  unsigned int TestFlag:1;
  /**
  *** \brief Print level.
  *** \details Which program messages to print to the console.
  **/
  unsigned int VerboseFlag:1;
  /**
  *** \brief Zip file process count.
  *** \details Number of zip file processes.
  **/
  int ProcessCount;
} OPTIONS_T;


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if     defined(__cplusplus)
extern "C" {
#endif  /* defined(__cplusplus) */

#if     defined(__cplusplus)
}
#endif  /* defined(__cplusplus) */


#endif    /* !defined(PIMZ_H) */


/**
*** pimz.h
**/
