/**
*** \file zipfile.h
*** \brief Test/convert a zip file.
*** \details Provides a method to test if a zip file is Torrent zipped, and
***   also provides a method to convert a zip file to a Torrent zipped file.
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(ZIPFILE_H)
/**
*** \internal
*** \brief zipfile.h identifier.
*** \details Identifier for zipfile.h.
**/
#define   ZIPFILE_H


/****
*****
***** INCLUDES
*****
****/

#include  <string>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

class MESSAGEMANAGER_C;


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if     defined(__cplusplus)
extern "C" {
#endif  /* defined(__cplusplus) */

/**
*** \brief Verify Torrent Zip file CRC32.
*** \details Verifies that the CRC32 of the Central Directory matches the CRC32
***   in the Torrent Zip file comment.
*** \param Pathname Pathname of the zip file to verify.
*** \retval true Torrent zip comment is current.
*** \retval false Torrent zip comment is not current.
**/
bool ZipFile_IsTorrentZipCommentCurrent(std::string const &Pathname);
/**
*** \brief Convert "normal" zip to Torrent zip.
*** \details Converts a "normal" zip file to a Torrent zip file.
*** \param Pathname Pathname of the zip file to convert.
*** \param MessageManager Message manager instance.
**/
void ZipFile_ConvertToTorrentZipFile(
    std::string const &Pathname,MESSAGEMANAGER_C &MessageManager);

#if     defined(__cplusplus)
}
#endif  /* defined(__cplusplus) */


#endif    /* !defined(ZIPFILE_H) */


/**
*** zipfile.h
**/
