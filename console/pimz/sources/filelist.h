/**
*** \file filelist.h
*** \brief
*** \details
*** \todo
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#if       !defined(FILELIST_H)
/**
*** \internal
*** \brief filelist.h identifier.
*** \details Identifier for filelist.h.
**/
#define   FILELIST_H


/****
*****
***** INCLUDES
*****
****/

#include  "messagemanager.h"
#include  "pimz.h"

#include  <boost/filesystem.hpp>
#include  <mutex>
#include  <set>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/

/** \todo **/
class FILELIST_C
{
  public:
    /**
    *** \brief Container data type.
    *** \details Type of data in the container.
    **/
    typedef boost::filesystem::path VALUE_C;
    /**
    *** \brief Container type.
    *** \details Type of container.
    **/
    typedef std::set<VALUE_C> TYPE_T;
    /**
    *** \brief Container constant iterator.
    *** \details Constant iterator for the container.
    **/
    typedef TYPE_T::const_iterator CONSTITERATOR_C;
    /**
    *** \brief Container iterator.
    *** \details Iterator for the container.
    **/
    typedef TYPE_T::size_type SIZE_T;

  public:
    /**
    *** \brief Default constructor.
    *** \details Default constructor.
    **/
    FILELIST_C(void);
    /**
    *** \brief Destructor.
    *** \details Destructor.
    **/
    ~FILELIST_C(void);
    /**
    *** \brief Add search path.
    *** \details Adds a search directory/filename to search.
    *** \param Path Directory/filename to search.
    **/
    void AddSearchPath(std::string const &Path);
    /**
    *** \brief Process a list of pathnames.
    *** \details Iterates through a list of zip files, checking if the CRC in
    ***   the Torrrent zip comment matches the CRC of the data in the zip file.
    ***   If the comment doesn't exist or is not correct, converts (possibly,
    ***   depends on the options) the zip to a Torrent zip.
    *** \param Options Processing options.
    *** \param MessageManager Message manager to receive messages.
    *** \retval 0 Success.
    *** \retval 1 Error.
    *** \retval 2 Internal error.
    **/
    EXITCODE_E Process(
        OPTIONS_T const &Options,MESSAGEMANAGER_C &MessageManager);
    /**
    *** \brief Provide exclusive data access.
    *** \details Provides exclusive data access to internal data.
    **/
    void Lock(void);
    /**
    *** \brief Filename count.
    *** \details Count of filenames in list.
    *** \returns Filename count.
    **/
    SIZE_T Size(void);
    /**
    *** \brief Remove filename.
    *** \details Removes a filename from the list.
    *** \returns Filename.
    **/
    std::string TakeFilename(void);
    /**
    *** \brief Remove exclusive data access.
    *** \details Removes exclusive data access to internal data.
    **/
    void Unlock(void);

  private:
    /**
    *** \brief Process directory.
    *** \details Processes a directory.
    *** \param Pathname Directory name.
    *** \retval 0 Success.
    *** \retval 1 Error.
    *** \retval 2 Internal error.
    **/
    EXITCODE_E HandleDirectory(VALUE_C const &Pathname);
    /**
    *** \brief Process normal file.
    *** \details Processes a normal file.
    *** \param Pathname Pathname of file.
    *** \retval 0 Success.
    *** \retval 1 Error.
    *** \retval 2 Internal error.
    **/
    EXITCODE_E HandleFile(VALUE_C const &Pathname);
    /**
    *** \brief Process "other".
    *** \details Processes an "other" (not a directory, file, or symbolic link).
    *** \param Pathname Pathname of "other".
    *** \retval 0 Success.
    *** \retval 1 Error.
    *** \retval 2 Internal error.
    **/
    EXITCODE_E HandleOther(VALUE_C const &Pathname);
    /**
    *** \brief Process symbolic link.
    *** \details Processes a symbolic link.
    *** \param Pathname Pathname of symbolic link.
    *** \retval 0 Success.
    *** \retval 1 Error.
    *** \retval 2 Internal error.
    **/
    EXITCODE_E HandleSymbolicLink(VALUE_C const &Pathname);
    /**
    *** \brief Process file or directory containing wildcards.
    *** \details Processes a file or directory containing wildcards.
    *** \param Pathname Pathname of wildcard.
    *** \retval 0 Success.
    *** \retval 1 Error.
    *** \retval 2 Internal error.
    **/
    EXITCODE_E HandleWildcard(VALUE_C const &Pathname);
    /**
    *** \param Pathname Pathname of file.
    *** \retval 0 Success.
    *** \retval 1 Error.
    *** \retval 2 Internal error.
    **/
    EXITCODE_E ProcessPathname(VALUE_C const &Pathname);

  private:
    /** \todo **/
    TYPE_T m_SearchPath;
    /** \todo **/
    TYPE_T m_FoundPathnames;
    /** \todo **/
    std::mutex m_Mutex;
};


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if     defined(__cplusplus)
extern "C" {
#endif  /* defined(__cplusplus) */

#if     defined(__cplusplus)
}
#endif  /* defined(__cplusplus) */


#endif    /* !defined(FILELIST_H) */


/**
*** filelist.h
**/
