/**
*** \file zipfile.cpp
*** \brief zipfile.cpp implementation.
*** \details Implementation file for zipfile.cpp.
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief zipfile.cpp identifier.
*** \details Identifier for zipfile.cpp.
**/
#define   ZIPFILE_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "zipfile.h"
#if       defined(DEBUG_ZIPFILE_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_ZIPFILE_CPP) */
#include  "awisetoolbox/generic/debuglog.h"

#include  "awisetoolbox/generic/sysdefs.h"
#include  "messagemanager.h"
#include  "minizipp.h"

#include  <algorithm>
#include  <boost/endian/conversion.hpp>
#include  <iomanip>
#include  <fstream>
#include  <sstream>
#include  <string.h>
#include  <vector>
#include  <zlib.h>

using namespace boost::endian;
using namespace MiniZip;
using namespace std;


/****
*****
***** DEFINES
*****
****/

/**
*** \brief Expected Torrent Zip comment size (in chars).
*** \details Expected size of comment for Torrent Zip files
***   ("TORRENTZIPPED-XXXXXXXX", where XXXXXXXX is the CRC32 of the Central
***   Directory.)
**/
#define   COMMENT_SIZE          (22)

/**
*** \brief End Of Central Directory value.
*** \details The value of the Signature field in the End Of Central Directory
***   structure.
**/
#define   SIGNATURE_EOCD  (0x06054b50)

/**
*** \brief First MAME release minute.
*** \details Minute of the first MAME release.
**/
#define   TM_MIN    (32)
/**
*** \brief First MAME release hour.
*** \details Hour of the first MAME release.
**/
#define   TM_HOUR   (23)
/**
*** \brief First MAME release day of the month.
*** \details Day of the month of the first MAME release.
**/
#define   TM_MDAY   (24)
/**
*** \brief First MAME release month.
*** \details Month of the first MAME release.
**/
#define   TM_MON    (11)
/**
*** \brief First MAME release year.
*** \details Year of the first MAME release.
**/
#define   TM_YEAR   (1996)

/**
*** \hideinitializer
*** \brief First MAME release DOS date.
*** \details The date of the first MAME release, in DOS format.
**/
#define   DOS_DATE  ((TM_YEAR - 80) << 25) | \
                        ((TM_MON+1) << 21) | \
                        (TM_MDAY << 16) | \
                        (TM_HOUR << 11) | \
                        (TM_MIN << 5) | \
                        (0 >> 1)


/****
*****
***** DATA TYPES
*****
****/

#pragma   pack(2)
/**
*** \brief End Of Central Directory format.
*** \details Format of the End Of Central Directory record of a zip file.
**/
typedef struct structEOCD
{
  /**
  *** \brief End Of Central Directory signature.
  *** \details The End Of Central Directory signature (0x06054b50).
  **/
  unsigned Signature;
  /**
  *** \brief Disk number.
  *** \details The number of this disk.
  **/
  unsigned short DiskNumber;
  /**
  *** \brief Central Directory start disk number.
  *** \details The number of the disk on which the central directory starts.
  **/
  unsigned short CentralDirectoryStartDiskNumber;
  /**
  *** \brief Central Directory entries count.
  *** \details The number of central directory entries on this disk.
  **/
  unsigned short DiskCentralDirectoryCount;
  /**
  *** \brief File count.
  *** \details The total number of files in the zip file. **/
  unsigned short CentralDirectoryCount;
  /**
  *** \brief Central Directory size (in bytes).
  *** \details The size (in bytes) of the entire central directory.
  **/
  unsigned CentralDirectorySize;
  /**
  *** \brief Central Directory start offset.
  *** \details Offset of the start of the central directory on the disk on
  ***   which the central directory starts.
  **/
  unsigned CentralDirectoryOffset;
  /**
  *** \brief Comment length.
  *** \details The length of the comment for this zip file.
  **/
  unsigned short CommentSize;
} EOCD_T;
#pragma   pack()

/**
*** \brief ROM data list.
*** \details List containing data about each ROM.
**/
using ROMLIST_C=list< pair<string,unsigned> >;


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

/**
*** \brief Read End Of Central Directure structure.
*** \details Reads the End Of Central Directure structure from a zip file into
***   memory.
*** \param Stream Input stream.
*** \param EOCD Storage for the End of Central Directory data.
*** \returns Input stream.
**/
inline static ifstream & operator>>(std::ifstream &Stream,EOCD_T &EOCD)
{
  DEBUGLOG_Printf2("operator>>(%p,EOCD_T:%p)",&Stream,&EOCD);
  DEBUGLOG_LogIn();

  Stream.read(reinterpret_cast<char*>(&EOCD),sizeof(EOCD));

  // These two are all that is needed.
  little_to_native_inplace(EOCD.Signature);
  little_to_native_inplace(EOCD.CentralDirectorySize);

  DEBUGLOG_LogOut();
  return(Stream);
}

/**
*** \brief Compute the Central Directory CRC32.
*** \details Compute the CRC32 of the Central Directory. The Minizip library
***   doesn't provide functions that are low level enough, so use the zip file
***   format documentation (http://www.pkware.com/appnote) to find and read the
***   End Of Central Directory structure. Using the information contained
***   therein, seek to the Central Directory, read it into memory, and compute
***   the CRC32.
*** \param Pathname Pathname of the zip file.
*** \param CommentSize Size of the global comment.
*** \returns CRC32 of the zip file Central Directory.
**/
static unsigned CalculateCRC32(string const &Pathname,unsigned CommentSize)
{
  ifstream File;
  EOCD_T EOCD;
  vector<char> CentralDirectory;
  unsigned CRC32;


  DEBUGLOG_Printf3(
      "CalculateCRC32(%p(%s),%d)",&Pathname,Pathname.c_str(),CommentSize);
  DEBUGLOG_LogIn();

  try
  {
    /* Parameter checking. */
    if (Pathname.empty()==true)
      throw(invalid_argument("empty pathname."));

    /* Open the file (binary mode). */
    File.open(Pathname,ios_base::binary);
    if (!File)
      throw(runtime_error("unable to open file."));

    /* The global comment is at the end of the file, so seek to the end of the
        file minus the size of the comment and minus the size of the End Of
        Central Directory structure. */
    if (File.seekg(-(signed)(sizeof(EOCD_T)+CommentSize),ios_base::end).fail())
      throw(runtime_error("unable to file seek."));

    /* Read the End Of Central Directory structure. */
    File >> EOCD;
    if (File.fail())
      throw(runtime_error("unable to read End of Central Directory."));

    /* Data sanity check. */
    if (EOCD.Signature!=SIGNATURE_EOCD)
      throw(runtime_error("invalid End of Central Directory signature."));

    /* Reserve space for the End Of Central Directory record. */
    CentralDirectory.reserve(EOCD.CentralDirectorySize);

    /* Seek to the start of the Central Directory. */
    if (File.seekg(EOCD.CentralDirectoryOffset,ios_base::beg).fail())
      throw(runtime_error("unable to file seek."));

    /* Read the Central Directory. */
    if (File.read(&CentralDirectory[0],EOCD.CentralDirectorySize).fail())
      throw(runtime_error("unable to read Central Directory."));

    /* Close the file. */
    File.close();
    if (File.fail())
      throw(runtime_error("unable to close file."));

    /* Calculate the CRC. */
    CRC32=(unsigned)crc32(
        crc32(0,NULL,0),(Bytef*)&CentralDirectory[0],EOCD.CentralDirectorySize);
  }
  catch(...)
  {
    DEBUGLOG_LogOut();
    throw;
  }

  DEBUGLOG_LogOut();
  return(CRC32);
}

/**
*** \brief CRC32 to Torrent Zip comment.
*** \details Formats a CRC32 value to a Torrent Zip comment string.
*** \param CRC32 - CRC32 value.
*** \returns Torrent Zip comment.
**/
inline static string FormatZipComment(unsigned CRC32)
{
  stringstream Comment;


  DEBUGLOG_Printf1("FormatZipComment(%08X)",CRC32);
  DEBUGLOG_LogIn();

  Comment.setf(ios::uppercase);
  Comment << "TORRENTZIPPED-" << setfill('0') << setw(8) << hex <<  CRC32;

  DEBUGLOG_LogOut();
  return(Comment.str());
}

void ZipFile_ConvertToTorrentZipFile(
    std::string const &Pathname,MESSAGEMANAGER_C &MessageManager)
{
  UNZIPFILE_C ZInputFile;
  unz_file_info FileInfo;
  zip_fileinfo DateAndFileAttributes;
  ROMLIST_C ROMList;
  string ROMPathname;
  int ZReturn;
  string TmpPathname;
  ZIPFILE_C ZOutputFile;
  MESSAGEMANAGER_C::MESSAGEBUFFER_C Messages;
  unsigned BufferSize;
  vector<char> Buffer;
  int ReadSize;
  unsigned int CRC32;


  DEBUGLOG_Printf2(
      "ZipFile_ConvertToTorrentZipFile(%p(%s))",&Pathname,Pathname.c_str());
  DEBUGLOG_LogIn();

  try
  {
    /* Parameter checking. */
    if (Pathname.empty()==true)
      throw(invalid_argument("empty pathname."));

    /* Open the file. */
    if (ZInputFile.Open(Pathname.c_str())!=UNZ_OK)
      throw(runtime_error("unable to open input file."));

    /* Loop through each ROM file in the zip file. */
    if (ZInputFile.GoToFirstFile()!=UNZ_OK)
      throw(runtime_error("unable to find input ROM file."));
    do
    {
      /* Get the file information. */
      if (ZInputFile.GetCurrentFileInfo(&FileInfo,&ROMPathname)!=UNZ_OK)
        throw(runtime_error("unable to read input ROM file information."));

      /* Save the information. */
      ROMList.push_back(make_pair(ROMPathname,FileInfo.compressed_size));

      /* Next file. */
      ZReturn=ZInputFile.GoToNextFile();
      if ( (ZReturn!=UNZ_OK) && (ZReturn!=UNZ_END_OF_LIST_OF_FILE) )
        throw(runtime_error("unable to find input ROM file."));
    }
    while(ZReturn!=UNZ_END_OF_LIST_OF_FILE);

    /* "Normalize" ROM names. All lowercase, only '/' directory separators. */
    for(ROMLIST_C::iterator ppIt=ROMList.begin();ppIt!=ROMList.end();++ppIt)
    {
      /* Convert '\' to '/'. */
      replace(ppIt->first.begin(),ppIt->first.end(),'\\','/');

      /* Lowercase. */
      transform(ppIt->first.begin(),ppIt->first.end(),
          ppIt->first.begin(),::tolower);
    }

    /* Now sort. */
    ROMList.sort();

    /* Temporary pathname for new zip file output. */
    TmpPathname=Pathname+".tmp";

    /* Open the output file. */
    if (ZOutputFile.Open(TmpPathname,ZIPFILE_C::APPEND_CREATE)!=Z_OK)
      throw(runtime_error("unable to open output file."));

    /* For each ROM file, transfer the filename, info, and data to the output
        file. */
    for(ROMLIST_C::iterator ppIt=ROMList.begin();ppIt!=ROMList.end();++ppIt)
    {
      Messages.VerboseInformation(Pathname+": adding \""+ppIt->first+"\".");
      MessageManager.TakeMessages(Messages);

      /* Find the ROM data via the ROM name. */
      if (ZInputFile.LocateFile(
          ppIt->first.c_str(),UNZIPFILE_C::CASE_INSENSITIVE)!=UNZ_OK)
        throw(runtime_error("unable to find input ROM file."));

      /* Set it as the current file in the input zip file. */
      if (ZInputFile.OpenCurrentFile()!=UNZ_OK)
        throw(runtime_error("unable to open input ROM file."));

      /* Get info about the current file. */
      if (ZInputFile.GetCurrentFileInfo(
          &FileInfo)!=UNZ_OK)
        throw(runtime_error("unable to read input ROM file information."));

      /* Initialize the ROM file in the output zip. */
      memset(&DateAndFileAttributes,0,sizeof(DateAndFileAttributes));
#if   (MINIZIP_VERSION_MAJOR==1) && (MINIZIP_VERSION_MINOR<2)
      DateAndFileAttributes.tmz_date.tm_min=TM_MIN;
      DateAndFileAttributes.tmz_date.tm_hour=TM_HOUR;
      DateAndFileAttributes.tmz_date.tm_mday=TM_MDAY;
      DateAndFileAttributes.tmz_date.tm_mon=TM_MON;
      DateAndFileAttributes.tmz_date.tm_year=TM_YEAR;
#else
      DateAndFileAttributes.dos_date=DOS_DATE;
#endif
      if (ZOutputFile.OpenNewFileInZip(
          ppIt->first,&DateAndFileAttributes)!=UNZ_OK)
        throw(runtime_error("unable to open output ROM file."));

      /* Allocate a buffer the size of the file. If the allocation fails,
          keep trying in progressively smaller amounts. */
      BufferSize=ppIt->second;
      do
      {
        try
        {
          Buffer.reserve(BufferSize);
        }
        catch(bad_alloc const &BA)
        {
          UNUSED(BA);
          BufferSize/=2;
          if (BufferSize<16384)
            throw;  // "What we have here is a failure to allocate."
        }
      }
      while(Buffer.capacity()<BufferSize);

      /* Copy the ROM file data (in chunks if necessary). */
      do
      {
        ReadSize=ZInputFile.ReadCurrentFile(&Buffer[0],BufferSize);
        if (ReadSize<0)
          throw(runtime_error("unable to read input ROM file."));
        if (ZOutputFile.WriteInFileInZip(&Buffer[0],ReadSize)!=Z_OK)
          throw(runtime_error("unable to write output ROM file."));
      }
      while(ReadSize!=0);

      /* Close the output ROM file. */
      if (ZOutputFile.CloseFileInZip()!=Z_OK)
        throw(runtime_error("unable to close output ROM file."));

      /* Close the input ROM file. */
      if (ZInputFile.CloseCurrentFile()!=UNZ_OK)
        throw(runtime_error("unable to close input ROM file."));
    }

    if (ZOutputFile.Close("")!=UNZ_OK)
      throw(runtime_error("unable to close output file."));

    if (ZInputFile.Close()!=UNZ_OK)
      throw(runtime_error("unable to close input file."));

    /* Update the CRC32. */
    CRC32=CalculateCRC32(TmpPathname,0);

    /* Just open the output file file... */
    if (ZOutputFile.Open(TmpPathname.c_str(),ZIPFILE_C::APPEND_ADDINZIP)!=Z_OK)
      throw(runtime_error("unable to open output file."));

    /* ...and the close it with the new CRC32. */
    if (ZOutputFile.Close(FormatZipComment(CRC32).c_str())!=UNZ_OK)
      throw(runtime_error("unable to close output file."));

    /* Overwrite the old zip with the temporary file. */
    if (rename(TmpPathname.c_str(),Pathname.c_str())==-1)
      throw(runtime_error("unable to overwrite original zip file."));
  }
  catch(...)
  {
    if (TmpPathname.empty()!=true)
      unlink(TmpPathname.c_str());  // Don't care about failure.
    DEBUGLOG_LogOut();
    throw;
  }

  DEBUGLOG_LogOut();
  return;
}

bool ZipFile_IsTorrentZipCommentCurrent(std::string const &Pathname)
{
  UNZIPFILE_C ZFile;
  bool CurrentFlag;
  unz_global_info GlobalInfo;
  string Comment;
  unsigned CRC;


  DEBUGLOG_Printf2(
      "ZipFile_IsTorrentZipCommentCorrect(%p(%s))",&Pathname,Pathname.c_str());
  DEBUGLOG_LogIn();

  try
  {
    /* Parameter checking. */
    if (Pathname.empty()==true)
      throw(invalid_argument("empty pathname."));

    /* Open the file. */
    if (ZFile.Open(Pathname)!=UNZ_OK)
      throw(runtime_error("unable to open file."));

    /* Assume not current. */
    CurrentFlag=false;

    /* Read the comment size. */
    if (ZFile.GetGlobalInfo(&GlobalInfo)!=UNZ_OK)
      throw(runtime_error("unable to read global information."));

    /* The comment should be exactly 22 bytes. If not, it is not current. */
    if (GlobalInfo.size_comment==COMMENT_SIZE)
    {
      /* Read the comment. */
      if (ZFile.GetGlobalComment(Comment)<0)
        throw(runtime_error("unable to read global comment."));

      /* Calculate the CRC32. */
      CRC=CalculateCRC32(Pathname,(unsigned)GlobalInfo.size_comment);

      /* Compare CRC32 to Torrent Zip comment. If same, they're current. */
      if (FormatZipComment(CRC).compare(Comment)==0)
        CurrentFlag=true;
    }

    /* Close the file. */
    if (ZFile.Close()!=UNZ_OK)
        throw(runtime_error("unable to close input file."));
  }
  catch(...)
  {
    DEBUGLOG_LogOut();
    throw;
  }

  DEBUGLOG_LogOut();
  return(CurrentFlag);
}


#undef    ZIPFILE_CPP


/**
*** zipfile.cpp
**/
