/**
*** \file messagemanager.cpp
*** \brief messagemanager.cpp implementation.
*** \details Implementation file for messagemanager.cpp.
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief messagemanager.cpp identifier.
*** \details Identifier for messagemanager.cpp.
**/
#define   MESSAGEMANAGER_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "messagemanager.h"
#if       defined(DEBUG_MESSAGEMANAGER_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_MESSAGEMANAGER_CPP) */
#include  "awisetoolbox/generic/debuglog.h"
#include  "awisetoolbox/generic/messagelog.h"
#include  "awisetoolbox/generic/sysdefs.h"

#include  "config.h"

#include  <algorithm>
#include  <fstream>
#include  <iostream>

using namespace std;


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

MESSAGEMANAGER_C::MESSAGELIST_C::MESSAGELIST_C(void)
{
  DEBUGLOG_Printf0("MESSAGEMANAGER_C::MESSAGELIST_C::MESSAGELIST_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

MESSAGEMANAGER_C::MESSAGELIST_C::~MESSAGELIST_C(void)
{
  DEBUGLOG_Printf0("MESSAGEMANAGER_C::MESSAGELIST_C::~MESSAGELIST_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

void MESSAGEMANAGER_C::MESSAGELIST_C::Append(MESSAGELIST_C &List)
{
  DEBUGLOG_Printf1("MESSAGEMANAGER_C::MESSAGELIST_C::Append(%p)",&List);
  DEBUGLOG_LogIn();

  Lock();
  m_Messages.splice(m_Messages.end(),List.m_Messages);
  Unlock();

  DEBUGLOG_LogOut();
  return;
}

inline MESSAGEMANAGER_C::MESSAGELIST_C::ITERATOR_C
    MESSAGEMANAGER_C::MESSAGELIST_C::Begin(void)
{
  MESSAGEMANAGER_C::MESSAGELIST_C::ITERATOR_C pIt;


  DEBUGLOG_Printf0("MESSAGEMANAGER_C::MESSAGELIST_C::Begin()");
  DEBUGLOG_LogIn();

  Lock();
  pIt=m_Messages.begin();
  Unlock();

  DEBUGLOG_LogOut();
  return(pIt);
}

void MESSAGEMANAGER_C::MESSAGELIST_C::Clear(OUTPUT_F Flags)
{
  DEBUGLOG_Printf1("MESSAGEMANAGER_C::MESSAGELIST_C::Clear(%08X)",Flags);
  DEBUGLOG_LogIn();


  Lock();

  /* Clear the flag. */
  transform(m_Messages.begin(),m_Messages.end(),m_Messages.begin(),
      [Flags](VALUE_C &Message)
      {
        get<2>(Message)=static_cast<OUTPUT_F>(get<2>(Message)&~Flags);
        return(Message);
      });

  /* Remove any messages with flags equal to 0. */
  m_Messages.remove_if(
      [](VALUE_C &Message)
      {
        return(get<2>(Message)==0);
      });

  Unlock();

  DEBUGLOG_LogOut();
  return;
}

inline MESSAGEMANAGER_C::MESSAGELIST_C::ITERATOR_C
    MESSAGEMANAGER_C::MESSAGELIST_C::End(void)
{
  MESSAGEMANAGER_C::MESSAGELIST_C::ITERATOR_C pIt;


  DEBUGLOG_Printf0("MESSAGEMANAGER_C::MESSAGELIST_C::End()");
  DEBUGLOG_LogIn();

  Lock();
  pIt=m_Messages.end();
  Unlock();

  DEBUGLOG_LogOut();
  return(pIt);
}

inline void MESSAGEMANAGER_C::MESSAGELIST_C::Lock(void)
{
  DEBUGLOG_Printf0("MESSAGEMANAGER_C::MESSAGELIST_C::Lock()");
  DEBUGLOG_LogIn();

  m_Mutex.lock();

  DEBUGLOG_LogOut();
  return;
}

void MESSAGEMANAGER_C::MESSAGELIST_C::
    PushBack(MESSAGETYPE_E Type,std::string const &Message)
{
  DEBUGLOG_Printf3("MESSAGEMANAGER_C::MESSAGELIST_C::PushBack(%d,%p(%s))",
      Type,&Message,Message.c_str());
  DEBUGLOG_LogIn();

  Lock();
  m_Messages.push_back(std::tuple<MESSAGETYPE_E,std::string,OUTPUT_F>(
      Type,Message,(OUTPUT_F)(OUTPUT_TERMINAL|OUTPUT_FILE)));
  Unlock();

  DEBUGLOG_LogOut();
  return;
}

inline void MESSAGEMANAGER_C::MESSAGELIST_C::Unlock(void)
{
  DEBUGLOG_Printf0("MESSAGEMANAGER_C::MESSAGELIST_C::Unlock()");
  DEBUGLOG_LogIn();

  m_Mutex.unlock();

  DEBUGLOG_LogOut();
  return;
}

MESSAGEMANAGER_C::MESSAGEBUFFER_C::MESSAGEBUFFER_C(void)
{
  DEBUGLOG_Printf0("MESSAGEMANAGER_C::MESSAGEBUFFER_C::MESSAGEBUFFER_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

MESSAGEMANAGER_C::MESSAGEBUFFER_C::~MESSAGEBUFFER_C(void)
{
  DEBUGLOG_Printf0("MESSAGEMANAGER_C::MESSAGEBUFFER_C::~MESSAGEBUFFER_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

void MESSAGEMANAGER_C::MESSAGEBUFFER_C::Error(std::string const &Message)
{
  DEBUGLOG_Printf2("MESSAGEMANAGER_C::MESSAGEBUFFER_C::Error(%p(%s))",
      &Message,Message.c_str());
  DEBUGLOG_LogIn();

  SaveMessage(MESSAGETYPE_ERROR,Message);

  DEBUGLOG_LogOut();
  return;
}

void MESSAGEMANAGER_C::MESSAGEBUFFER_C::Information(std::string const &Message)
{
  DEBUGLOG_Printf2("MESSAGEMANAGER_C::MESSAGEBUFFER_C::Information(%p(%s))",
      &Message,Message.c_str());
  DEBUGLOG_LogIn();

  SaveMessage(MESSAGETYPE_INFORMATION,Message);

  DEBUGLOG_LogOut();
  return;
}

void MESSAGEMANAGER_C::MESSAGEBUFFER_C::
    InternalError(std::string const &Filename,int LineNumber)
{
  string Msg;


  DEBUGLOG_Printf3(
      "MESSAGEMANAGER_C::MESSAGEBUFFER_C::InternalError(%p(%s),%d)",
      &Filename,Filename.c_str(),LineNumber);
  DEBUGLOG_LogIn();

  Msg=string(PIMZ_EXECUTABLENAME) + string(PIMZ_VERSION);
  Msg+=", " + Filename + ", " + to_string(LineNumber) + ".";
  SaveMessage(MESSAGETYPE_ERROR,Msg);

  DEBUGLOG_LogOut();
  return;
}

void MESSAGEMANAGER_C::MESSAGEBUFFER_C::SaveMessage(
    MESSAGETYPE_E Type,std::string const &Message)
{
  string Prefix;


  DEBUGLOG_Printf3("MESSAGEMANAGER_C::MESSAGEBUFFER_C::SaveMessage(%u,%p(%s))",
      Type,&Message,Message.c_str());
  DEBUGLOG_LogIn();

  try
  {
    switch(Type)
    {
      case MESSAGETYPE_ERROR:
        Prefix="Error: ";
        break;
      case MESSAGETYPE_INFORMATION:
        Prefix="";
        break;
      case MESSAGETYPE_INTERNALERROR:
        Prefix="Internal error: ";
        break;
      case MESSAGETYPE_VERBOSEINFORMATION:
        Prefix="";
        break;
      case MESSAGETYPE_WARNING:
        Prefix="Warning: ";
        break;
      default:
        throw(invalid_argument("unknown message type."));
        break;
    }

    m_Messages.PushBack(Type,Prefix+Message+"\n");
  }
  catch(invalid_argument const &IAException)
  {
    UNUSED(IAException);
    MESSAGELOG_Error(IAException.what());
  }

  DEBUGLOG_LogOut();
  return;
}

void MESSAGEMANAGER_C::MESSAGEBUFFER_C::
    VerboseInformation(std::string const &Message)
{
  DEBUGLOG_Printf2(
      "MESSAGEMANAGER_C::MESSAGEBUFFER_C::VerboseInformation(%p(%s))",
      &Message,Message.c_str());
  DEBUGLOG_LogIn();

  SaveMessage(MESSAGETYPE_VERBOSEINFORMATION,Message);

  DEBUGLOG_LogOut();
  return;
}

void MESSAGEMANAGER_C::MESSAGEBUFFER_C::Warning(std::string const &Message)
{
  DEBUGLOG_Printf2("MESSAGEMANAGER_C::MESSAGEBUFFER_C::Warning(%p(%s))",
      &Message,Message.c_str());
  DEBUGLOG_LogIn();

  SaveMessage(MESSAGETYPE_WARNING,Message);

  DEBUGLOG_LogOut();
  return;
}

MESSAGEMANAGER_C::MESSAGEMANAGER_C(void)
{
  DEBUGLOG_Printf0("MESSAGEMANAGER_C::MESSAGEMANAGER_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

MESSAGEMANAGER_C::~MESSAGEMANAGER_C(void)
{
  DEBUGLOG_Printf0("MESSAGEMANAGER_C::~MESSAGEMANAGER_C()");
  DEBUGLOG_LogIn();

  DEBUGLOG_LogOut();
  return;
}

void MESSAGEMANAGER_C::Print(bool VerboseFlag)
{
  DEBUGLOG_Printf1("MESSAGEMANAGER_C::Print(%u)",VerboseFlag);
  DEBUGLOG_LogIn();

  /* Print the messages. */
  for(MESSAGELIST_C::CONSTITERATOR_C
      pIt=m_Messages.Begin();pIt!=m_Messages.End();++pIt)
  {
    if (get<2>(*pIt)&OUTPUT_TERMINAL)
    {
      /* Errors/warnings. */
      if ( (get<0>(*pIt)!=MESSAGETYPE_INFORMATION) &&
          (get<0>(*pIt)!=MESSAGETYPE_VERBOSEINFORMATION) )
        cerr << get<1>(*pIt);
      else if ( (get<0>(*pIt)==MESSAGETYPE_INFORMATION) ||
          ((get<0>(*pIt)==MESSAGETYPE_VERBOSEINFORMATION) && (VerboseFlag)) )
        cout << get<1>(*pIt);
    }
  }

  /* Clear the terminal flag. */
  m_Messages.Clear(OUTPUT_TERMINAL);

  DEBUGLOG_LogOut();
  return;
}

void MESSAGEMANAGER_C::Save(void)
{
  DEBUGLOG_Printf0("MESSAGEMANAGER_C::Save()");
  DEBUGLOG_LogIn();

  try
  {
    ofstream LogFile;


    if (!m_LogFilePathname.empty())
    {
      LogFile.open(m_LogFilePathname,ios::app|ios::binary|ios::out);
      if (!LogFile)
        throw(runtime_error("unable to open log file."));

      for(MESSAGELIST_C::CONSTITERATOR_C
          pIt=m_Messages.Begin();pIt!=m_Messages.End();++pIt)
        if (get<2>(*pIt)&OUTPUT_FILE)
          LogFile << get<1>(*pIt);

      LogFile.close();
    }
  }
  catch(runtime_error const &REException)
  {
    UNUSED(REException);
    MESSAGELOG_Error(REException.what());
  }

  /* Clear the file flag. */
  m_Messages.Clear(OUTPUT_FILE);  // If an error, log messages are discarded.

  DEBUGLOG_LogOut();
  return;
}

void MESSAGEMANAGER_C::SetLogFilePathname(std::string const &Pathname)
{
  ofstream LogFile;


  DEBUGLOG_Printf2(
      "MESSAGEMANAGER_C::SetLogFilePathname(%p(%s))",&Pathname,Pathname.c_str());
  DEBUGLOG_LogIn();

  try
  {
    if (Pathname.empty())
      throw(invalid_argument("invalid filename."));
    m_LogFilePathname=Pathname;

    /* "Initialize" the log file. */
    LogFile.open(m_LogFilePathname,ios::binary|ios::out|ios::trunc);
    if (!LogFile)
      throw(runtime_error("unable to create log file."));
    LogFile.close();
  }
  catch(invalid_argument const &IAException)
  {
    UNUSED(IAException);
    MESSAGELOG_Error(IAException.what());
  }
  catch(runtime_error const &REException)
  {
    UNUSED(REException);
    MESSAGELOG_Error(REException.what());
  }

  DEBUGLOG_LogOut();
  return;
}

void MESSAGEMANAGER_C::TakeMessages(MESSAGEBUFFER_C &MessageBuffer)
{
  DEBUGLOG_Printf1("MESSAGEMANAGER_C::TakeMessages(%p)",&MessageBuffer);
  DEBUGLOG_LogIn();

  /* Transfer the messages from the buffer. */
  MessageBuffer.m_Messages.Lock();
  m_Messages.Append(MessageBuffer.m_Messages);
  MessageBuffer.m_Messages.Unlock();

  /* If no log file, clear the output file flag. */
  if (m_LogFilePathname==string())
    m_Messages.Clear(OUTPUT_FILE);

  DEBUGLOG_LogOut();
  return;
}


#undef    MESSAGEMANAGER_CPP


/**
*** messagemanager.cpp
**/
