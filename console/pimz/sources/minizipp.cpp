/**
*** \file minizipp.cpp
*** \brief minizipp.cpp implementation.
*** \details Implementation file for minizipp.cpp.
**/

/*
** This file is part of PIMZ.
** Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
*** \internal
*** \brief minizipp.cpp identifier.
*** \details Identifier for minizipp.cpp.
**/
#define   MINIZIPP_CPP


/****
*****
***** INCLUDES
*****
****/

#include  "minizipp.h"
#if       defined(DEBUG_MINIZIPP_CPP)
#if       !defined(USE_DEBUGLOG)
#define   USE_DEBUGLOG
#endif    /* !defined(USE_DEBUGLOG) */
#endif    /* defined(DEBUG_MINIZIPP_CPP) */
#include  "awisetoolbox/generic/debuglog.h"

using namespace std;


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** FUNCTIONS
*****
****/

namespace MiniZip
{
#if       ( (MINIZIP_VERSION_MAJOR==2) || \
          ( (MINIZIP_VERSION_MAJOR==1) && (MINIZIP_VERSION_MINOR>=2) ) )
  static int CaseInsensitiveCompare
      (unzFile pFile,char const *pFilename0,char const *pFilename1)
  {
    UNUSED(pFile);


    DEBUGLOG_Printf5("CaseInsensitiveCompare(%p,%p(%s),%p(%s))",
        pFile,pFilename0,pFilename0,pFilename1,pFilename1);
    DEBUGLOG_LogIn();

    DEBUGLOG_LogOut();
#if       defined(_WIN32)
    return(stricmp(pFilename0,pFilename1));
#else     /* defined(_WIN32) */
    return(strcasecmp(pFilename0,pFilename1));
#endif    /* defined(_WIN32) */
  }

  static int CaseSensitiveCompare
      (unzFile pFile,char const *pFilename0,char const *pFilename1)
  {
    UNUSED(pFile);


    DEBUGLOG_Printf5("CaseSensitiveCompare(%p,%p(%s),%p(%s))",
        pFile,pFilename0,pFilename0,pFilename1,pFilename1);
    DEBUGLOG_LogIn();

    DEBUGLOG_LogOut();
    return(strcmp(pFilename0,pFilename1));
  }
#endif    /* ( (MINIZIP_VERSION_MAJOR==2) || \
             ((MINIZIP_VERSION_MAJOR==1) && (MINIZIP_VERSION_MINOR>=2)) ) */

  UNZIPFILE_C::UNZIPFILE_C(void)
  {
    DEBUGLOG_Printf0("UNZIPFILE_C::UNZIPFILE_C()");
    DEBUGLOG_LogIn();

    m_File=NULL;

    DEBUGLOG_LogOut();
    return;
  }

  UNZIPFILE_C::~UNZIPFILE_C(void)
  {
    DEBUGLOG_Printf0("UNZIPFILE_C::~UNZIPFILE_C()");
    DEBUGLOG_LogIn();

    Close();

    DEBUGLOG_LogOut();
    return;
  }

  UNZIPFILE_C::RETURN_E UNZIPFILE_C::Close(void)
  {
    RETURN_E Return;


    DEBUGLOG_Printf0("UNZIPFILE_C::Close()");
    DEBUGLOG_LogIn();

    if (m_File==NULL)
      Return=RETURN_OK;
    else
      Return=static_cast<RETURN_E>(unzClose(m_File));
    m_File=NULL;

    DEBUGLOG_LogOut();
    return(Return);
  }

  UNZIPFILE_C::RETURN_E UNZIPFILE_C::CloseCurrentFile(void)
  {
    DEBUGLOG_Printf0("UNZIPFILE_C::CloseCurrentFile()");
    DEBUGLOG_LogIn();

    DEBUGLOG_LogOut();
    return(static_cast<RETURN_E>(unzCloseCurrentFile(m_File)));
  }

  UNZIPFILE_C::RETURN_E UNZIPFILE_C::GetCurrentFileInfo(
      unz_file_info *pFileInfo,string *pPathname,string *pComment,
      void *pExtraField,uint16_t ExtraFieldSize)
  {
    RETURN_E Return;
    char *pPathnameBuffer;
    char *pCommentBuffer;


    DEBUGLOG_Printf4("UNZIPFILE_C::GetCurrentFileInfo(%p,%p,%p,%p)",
        pFileInfo,pPathname,pComment,pExtraField);
    DEBUGLOG_LogIn();

    /* Get basic information about the current file. */
    Return=static_cast<RETURN_E>
        (unzGetCurrentFileInfo(m_File,pFileInfo,NULL,0, NULL,0,NULL,0));

    /* Successful return, and caller wants additional data. */
    if ( (Return==RETURN_OK) && ((pPathname!=NULL) || (pComment!=NULL)) )
    {
      /* Allocate C compatible buffers as necessary. */
      if (pPathname==NULL)
        pPathnameBuffer=NULL;
      else
      {
        /* Allocate a C compatible buffer. */
        pPathname->clear();
        pPathname->resize(pFileInfo->size_filename+1,'\0');
        pPathnameBuffer=&((*pPathname)[0]);
      }
      if (pComment==NULL)
        pCommentBuffer=NULL;
      else
      {
        /* Allocate a C compatible buffer. */
        pComment->clear();
        pComment->resize(pFileInfo->size_file_comment+1,'\0');
        pCommentBuffer=&((*pComment)[0]);
      }

      /* Get the additional data. */
      Return=static_cast<RETURN_E>(unzGetCurrentFileInfo(
          m_File,pFileInfo,
          pPathnameBuffer,(uint16_t)(pFileInfo->size_filename+1),
          pExtraField,ExtraFieldSize,
          pCommentBuffer,(uint16_t)(pFileInfo->size_file_comment+1)));
      if (Return==RETURN_OK)
      {
        /* Copy the data to the return variables. */
        if (pComment!=NULL)
          pComment->erase(pComment->size()-1);
        if (pPathname!=NULL)
          pPathname->erase(pPathname->size()-1);
      }
    }

    DEBUGLOG_LogOut();
    return(Return);
  }

  UNZIPFILE_C::RETURN_E UNZIPFILE_C::GetGlobalComment(string &Comment)
  {
    RETURN_E Return;
    unz_global_info GlobalInfo;


    DEBUGLOG_Printf1("UNZIPFILE_C::GetGlobalComment(%p)",&Comment);
    DEBUGLOG_LogIn();

    /* Read the global information (contains comment size). */
    Return=GetGlobalInfo(&GlobalInfo);
    if (Return==RETURN_OK)
    {
      /* Allocate a C compatible buffer. */
      Comment.clear();
      Comment.resize(GlobalInfo.size_comment+1,'\0');

      /* Get the comment. */
      Return=static_cast<RETURN_E>(unzGetGlobalComment(
          m_File,&Comment[0],(uint16_t)(GlobalInfo.size_comment+1)));
      if (Return==((signed)GlobalInfo.size_comment))
      {
        Comment.erase(Comment.size()-1);
        Return=RETURN_OK;  // Don't return comment size.
      }
    }

    DEBUGLOG_LogOut();
    return(Return);
  }

  UNZIPFILE_C::RETURN_E UNZIPFILE_C::GetGlobalInfo(unz_global_info *pGlobalInfo)
  {
    DEBUGLOG_Printf1("UNZIPFILE_C::GetGlobalInfo(%p)",pGlobalInfo);
    DEBUGLOG_LogIn();

    DEBUGLOG_LogOut();
    return(static_cast<RETURN_E>(unzGetGlobalInfo(m_File,pGlobalInfo)));
  }

  UNZIPFILE_C::RETURN_E UNZIPFILE_C::GoToFirstFile(void)
  {
    DEBUGLOG_Printf0("UNZIPFILE_C::GoToFirstFile()");
    DEBUGLOG_LogIn();

    DEBUGLOG_LogOut();
    return(static_cast<RETURN_E>(unzGoToFirstFile(m_File)));
  }

  UNZIPFILE_C::RETURN_E UNZIPFILE_C::GoToNextFile(void)
  {
    DEBUGLOG_Printf0("UNZIPFILE_C::GoToNextFile()");
    DEBUGLOG_LogIn();

    DEBUGLOG_LogOut();
    return(static_cast<RETURN_E>(unzGoToNextFile(m_File)));
  }

  UNZIPFILE_C::RETURN_E
      UNZIPFILE_C::LocateFile(string const &Pathname,CASE_E CaseSensitivity)
  {
    UNZIPFILE_C::RETURN_E Return;


    DEBUGLOG_Printf3("UNZIPFILE_C::LocateFile(%p(%s),%d)",
        &Pathname,Pathname.c_str(),CaseSensitivity);
    DEBUGLOG_LogIn();

#if       (MINIZIP_VERSION_MAJOR==1) && (MINIZIP_VERSION_MINOR<2)
    Return=static_cast<RETURN_E>(
            unzLocateFile(m_File,Pathname.c_str(),CaseSensitivity));
#else
    switch(CaseSensitivity)
    {
#if       defined(_WIN32)
      case CASE_OSDEFAULT:
#endif    /* defined(_WIN32) */
      case CASE_INSENSITIVE:
        Return=static_cast<RETURN_E>(
            unzLocateFile(m_File,Pathname.c_str(),CaseInsensitiveCompare));
        break;

#if       !defined(_WIN32)
      case CASE_OSDEFAULT:
#endif    /* defined(_WIN32) */
      case CASE_SENSITIVE:
        Return=static_cast<RETURN_E>(
            unzLocateFile(m_File,Pathname.c_str(),CaseSensitiveCompare));
        break;
      default:
        Return=RETURN_PARAMERROR;
        break;
    }
#endif    /* (MINIZIP_VERSION_MAJOR==1) && (MINIZIP_VERSION_MINOR<2) */

    DEBUGLOG_LogOut();
    return(Return);
  }

  UNZIPFILE_C::RETURN_E UNZIPFILE_C::Open(string const &Pathname)
  {
    DEBUGLOG_Printf2(
        "UNZIPFILE_C::LocateFile(%p(%s))",&Pathname,Pathname.c_str());
    DEBUGLOG_LogIn();

    m_File=unzOpen(Pathname.c_str());

    DEBUGLOG_LogOut();
    return((m_File)?RETURN_OK:RETURN_ERRNO);
  }

  UNZIPFILE_C::RETURN_E UNZIPFILE_C::OpenCurrentFile(void)
  {
    DEBUGLOG_Printf0("UNZIPFILE_C::OpenCurrentFile()");
    DEBUGLOG_LogIn();

    DEBUGLOG_LogOut();
    return(static_cast<RETURN_E>(unzOpenCurrentFile(m_File)));
  }

  UNZIPFILE_C::RETURN_E
      UNZIPFILE_C::ReadCurrentFile(void *pBuffer,unsigned Length)
  {
    DEBUGLOG_Printf2("UNZIPFILE_C::ReadCurrentFile(%p,%u)",pBuffer,Length);
    DEBUGLOG_LogIn();

    DEBUGLOG_LogOut();
    return(static_cast<RETURN_E>(unzReadCurrentFile(m_File,pBuffer,Length)));
  }

  ZIPFILE_C::ZIPFILE_C(void)
  {
    DEBUGLOG_Printf0("ZIPFILE_C::ZIPFILE_C()");
    DEBUGLOG_LogIn();

    m_File=NULL;

    DEBUGLOG_LogOut();
    return;
  }

  ZIPFILE_C::~ZIPFILE_C(void)
  {
    DEBUGLOG_Printf0("ZIPFILE_C::~ZIPFILE_C()");
    DEBUGLOG_LogIn();

    Close();

    DEBUGLOG_LogOut();
    return;
  }

  ZIPFILE_C::RETURN_E ZIPFILE_C::Close(string const &GlobalComment)
  {
    RETURN_E Return;


    DEBUGLOG_Printf2(
        "ZIPFILE_C::Close(%p(%s))",&GlobalComment,GlobalComment.c_str());
    DEBUGLOG_LogIn();

    if (m_File==NULL)
      Return=RETURN_OK;
    else
      Return=static_cast<RETURN_E>(zipClose(m_File,GlobalComment.c_str()));
    m_File=NULL;

    DEBUGLOG_LogOut();
    return(Return);
  }

  ZIPFILE_C::RETURN_E ZIPFILE_C::CloseFileInZip(void)
  {
    DEBUGLOG_Printf0("ZIPFILE_C::CloseFileInZip()");
    DEBUGLOG_LogIn();

    DEBUGLOG_LogOut();
    return(static_cast<RETURN_E>(zipCloseFileInZip(m_File)));
  }

  ZIPFILE_C::RETURN_E ZIPFILE_C::Open(string const &Pathname,APPEND_E Append)
  {
    DEBUGLOG_Printf3(
        "ZIPFILE_C::Open(%p(%s),%d)",&Pathname,Pathname.c_str(),Append);
    DEBUGLOG_LogIn();

    m_File=zipOpen(Pathname.c_str(),static_cast<int>(Append));

    DEBUGLOG_LogOut();
    return((m_File)?RETURN_OK:RETURN_ERRNO);
  }

  ZIPFILE_C::RETURN_E ZIPFILE_C::OpenNewFileInZip(string const &Pathname,
      zip_fileinfo const *pZipFileInfo,string const &Comment,
      void const *pLocalExtraField,uint16_t LocalExtraFieldSize,
      void const *pGlobalExtraField,uint16_t GlobalExtraFieldSize,
      METHOD_E Method,LEVEL_E Level)
  {
    DEBUGLOG_Printf11(
        "ZIPFILE_C::OpenNewFileInZip(%p(%s),%p,%p(%s),%p,%u,%p,%u,%d,%d)",
        &Pathname,Pathname.c_str(),pZipFileInfo,&Comment,Comment.c_str(),
        pLocalExtraField,LocalExtraFieldSize,
        pGlobalExtraField,GlobalExtraFieldSize,Method,Level);
    DEBUGLOG_LogIn();

    DEBUGLOG_LogOut();
    return(static_cast<RETURN_E>(
        zipOpenNewFileInZip3(m_File,Pathname.c_str(),pZipFileInfo,
        pLocalExtraField,LocalExtraFieldSize,
        pGlobalExtraField,GlobalExtraFieldSize,Comment.c_str(),Method,Level,0,
        MAX_WBITS,DEF_MEM_LEVEL,Z_DEFAULT_STRATEGY,NULL,0)));
  }

  ZIPFILE_C::RETURN_E
      ZIPFILE_C::WriteInFileInZip(void const *pBuffer,unsigned Length)
  {
    DEBUGLOG_Printf2("ZIPFILE_C::WriteInFileInZip(%p,%u)",pBuffer,Length);
    DEBUGLOG_LogIn();

    DEBUGLOG_LogOut();
    return(static_cast<RETURN_E>(zipWriteInFileInZip(m_File,pBuffer,Length)));
  }
}


#undef    MINIZIPP_CPP


/**
*** minizipp.cpp
**/
