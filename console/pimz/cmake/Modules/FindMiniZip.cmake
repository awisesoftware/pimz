#
# FindMiniZip.cmake
# Attempt to find the minizip includes and libraries.
#

#
# This file is part of PIMZ.
# Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# Usage
# -----
#
# Use this module by invoking find_package with the form:
#  FIND_PACKAGE(MiniZip
#               [REQUIRED])         # Fail if MiniZip is not found.
#
#
# Resulting Variables
# -------------------
#
# MINIZIP_FOUND        - True if library found.
# MINIZIP_INCLUDE_DIRS - Where to find includes.
# MINIZIP_LIBRARIES    - List of libraries.
#
# MiniZip::MiniZip   - The MiniZip import target.
#


FIND_PACKAGE(PkgConfig QUIET)
IF(PkgConfig_FOUND)
  PKG_CHECK_MODULES(PC_MINIZIP QUIET MiniZip)
ENDIF()

FIND_PATH(MINIZIP_INCLUDE_DIR
    NAMES mz.h zip.h
    PATHS ${PC_MINIZIP_INCLUDE_DIRS}
    PATH_SUFFIXES minizip) # . ?
FIND_LIBRARY(MINIZIP_LIBRARY
    NAMES minizip libminizip
    PATHS ${PC_MINIZIP_LIBRARY_DIRS})

# Get the version.
IF(MINIZIP_INCLUDE_DIR)
  # Version 2.x library.
  IF(EXISTS "${MINIZIP_INCLUDE_DIR}/mz.h")
    # Find the relevant line.
    FILE(STRINGS
        "${MINIZIP_INCLUDE_DIR}/mz.h"
        MINIZIP_TEMP
        REGEX "#define[ \t]+MZ_VERSION[ \t]+\\\(\"[0-9]+\\.[0-9]+\\.[0-9]+\"\\\)")
    # Get the relevant data.
    STRING(REGEX REPLACE "^.*MZ_VERSION[ \t]+\\\(\"([0-9]+).*$" "\\1"
        MINIZIP_VERSION_MAJOR ${MINIZIP_TEMP})
    STRING(REGEX REPLACE "^.*MZ_VERSION[ \t]+\\\(\"[0-9]+\\.([0-9]+).*$" "\\1"
        MINIZIP_VERSION_MINOR ${MINIZIP_TEMP})
    STRING(REGEX REPLACE "^.*MZ_VERSION[ \t]+\\\(\"[0-9]+\\.[0-9]+\\.([0-9]+).*$" "\\1"
        MINIZIP_VERSION_PATCH ${MINIZIP_TEMP})
    SET(MINIZIP_VERSION_STRING
        "${MINIZIP_VERSION_MAJOR}.${MINIZIP_VERSION_MINOR}.${MINIZIP_VERSION_PATCH}")
    UNSET(MINIZIP_TEMP)
  ENDIF()
  # Version 1.x library.
  IF(EXISTS "${MINIZIP_INCLUDE_DIR}/zip.h")
    # Find the relevant line.
    FILE(STRINGS
        "${MINIZIP_INCLUDE_DIR}/zip.h"
        MINIZIP_TEMP
        REGEX "[ \t]+Version[ \t]+[0-9]+\\.[0-9]+")
    # Get the relevant data.
    STRING(REGEX REPLACE "^.*Version[ \t]+([0-9]+).*$" "\\1"
        MINIZIP_VERSION_MAJOR ${MINIZIP_TEMP})
    STRING(REGEX REPLACE "^.*Version[ \t]+[0-9]+\\.([0-9]+).*$" "\\1"
        MINIZIP_VERSION_MINOR ${MINIZIP_TEMP})
    SET(MINIZIP_VERSION_STRING
        "${MINIZIP_VERSION_MAJOR}.${MINIZIP_VERSION_MINOR}")
    UNSET(MINIZIP_TEMP)
  ENDIF()
ENDIF()

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(MiniZip
    FOUND_VAR MINIZIP_FOUND
    REQUIRED_VARS MINIZIP_LIBRARY MINIZIP_INCLUDE_DIR
    VERSION_VAR MINIZIP_VERSION_STRING)

IF(MINIZIP_FOUND)
  SET(MINIZIP_LIBRARIES ${MINIZIP_LIBRARY})
  SET(MINIZIP_INCLUDE_DIRS ${MINIZIP_INCLUDE_DIR})
  SET(MINIZIP_DEFINITIONS ${PC_MINIZIP_CFLAGS_OTHER})
ENDIF()

IF(MINIZIP_FOUND AND NOT TARGET MiniZip::MiniZip)
  ADD_LIBRARY(MiniZip::MiniZip UNKNOWN IMPORTED)
  SET_TARGET_PROPERTIES(MiniZip::MiniZip
      PROPERTIES
        IMPORTED_LOCATION "${MINIZIP_LIBRARY}"
        INTERFACE_COMPILE_OPTIONS "${PC_MINIZIP_CFLAGS_OTHER}"
        INTERFACE_INCLUDE_DIRECTORIES "${MINIZIP_INCLUDE_DIR}")
ENDIF()

MARK_AS_ADVANCED(MINIZIP_INCLUDE_DIR MINIZIP_LIBRARY)


#
# FindMiniZip.cmake
#
