#
# FindArgtable2.cmake
# Attempt to find the argtable2 includes and libraries.
#

#
# This file is part of PIMZ.
# Copyright (C) 2018-2019 by Alan Wise <awisesoftware@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# Usage
# -----
#
# Use this module by invoking find_package with the form:
#  FIND_PACKAGE(Argtable2
#               [REQUIRED])         # Fail if Argtable2 is not found.
#
#
# Resulting Variables
# -------------------
#
# ARGTABLE2_FOUND        - True if library found.
# ARGTABLE2_INCLUDE_DIRS - Where to find includes.
# ARGTABLE2_LIBRARIES    - List of libraries.
#
# Argtable2::Argtable2   - The Argtable2 import target.
#


FIND_PACKAGE(PkgConfig QUIET)
IF(PkgConfig_FOUND)
  PKG_CHECK_MODULES(PC_ARGTABLE2 QUIET Argtable2)
ENDIF()

FIND_PATH(ARGTABLE2_INCLUDE_DIR
    NAMES argtable2.h
    PATHS ${PC_ARGTABLE2_INCLUDE_DIRS}
    PATH_SUFFIXES argtable argtable2)
FIND_LIBRARY(ARGTABLE2_LIBRARY
    NAMES argtable2
    PATHS ${PC_ARGTABLE2_LIBRARY_DIRS})

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Argtable2
    FOUND_VAR ARGTABLE2_FOUND
    REQUIRED_VARS ARGTABLE2_LIBRARY ARGTABLE2_INCLUDE_DIR)

IF(ARGTABLE2_FOUND)
  SET(ARGTABLE2_LIBRARIES ${ARGTABLE2_LIBRARY})
  SET(ARGTABLE2_INCLUDE_DIRS ${ARGTABLE2_INCLUDE_DIR})
  SET(ARGTABLE2_DEFINITIONS ${PC_ARGTABLE2_CFLAGS_OTHER})
ENDIF()

IF(ARGTABLE2_FOUND AND NOT TARGET Argtable2::Argtable2)
  ADD_LIBRARY(Argtable2::Argtable2 UNKNOWN IMPORTED)
  SET_TARGET_PROPERTIES(Argtable2::Argtable2
      PROPERTIES
        IMPORTED_LOCATION "${ARGTABLE2_LIBRARY}"
        INTERFACE_COMPILE_OPTIONS "${PC_ARGTABLE2_CFLAGS_OTHER}"
        INTERFACE_INCLUDE_DIRECTORIES "${ARGTABLE2_INCLUDE_DIR}")
ENDIF()

MARK_AS_ADVANCED(ARGTABLE2_INCLUDE_DIR ARGTABLE2_LIBRARY)


#
# FindArgtable.cmake
#
