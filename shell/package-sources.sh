#!/bin/bash

# $1 - Project directory (required).
# $2 - Project (required).
# $3 - Version (required).

if [ "$#" -ne "3" ]; then
  if [ "$#" -lt "3" ]; then
    echo "Error: missing parameters."
  else
    echo "Error: extra parameters."
  fi

  echo "Usage: $0 [projectdir] [projectname] [projectversion]"
  exit 1
fi

# Make script easier to read.
ProjDir="$1"
Project="$2"
Version="$3"

# Create source packages.
rm -rf .git "/tmp/${Project}-${Version}" >/dev/null 2>&1
mkdir -p "/tmp/${Project}-${Version}" &&
cp -r . "/tmp/${Project}-${Version}" &&
cd "/tmp" &&
tar czf  "${ProjDir}/${Project}-${Version}.tar.gz" "${Project}-${Version}" &&
zip -qr9 "${ProjDir}/${Project}-${Version}.zip"    "${Project}-${Version}" &&
cd "${ProjDir}"
