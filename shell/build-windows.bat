@echo off

:: Unable to pass environment variables from child process to parent,
::  so the script is inlined.

:: windows-env.bat %1


:: Parameter checking.
if "x86_64"=="" (
  echo
  echo Error: missing architecture.
  exit /b 1
)

:: Check environment vars.
if "%VCVARS_ROOT_DIR%"=="" (
  echo
  echo Error: 'VCVARS_ROOT_DIR' not set.
  exit /b 1
)
if "%PACKAGES_ROOT_DIR%"=="" (
  echo
  echo Error: 'PACKAGES_ROOT_DIR' not set.
  exit /b 1
)

:: Set up architecture variable.
setlocal
set Arch=
if "x86_64"=="x86" (
  set Arch=x86
  set OneOfThree=x86
)
if "x86_64"=="x86_64" (
  set Arch=amd64
  set OneOfThree=x64
)

if "%Arch%"=="" (
  echo.
  echo Error: unsupported architecture ^(x86_64^). Use "x86" or "x86_64".
  exit /b 1
)

call "%VCVARS_ROOT_DIR%\vcvarsall.bat" %Arch%
if ERRORLEVEL 1 exit /b 1
set CMAKE_PREFIX_PATH=%CMAKE_PREFIX_PATH%;%PACKAGES_ROOT_DIR%\x86_64
set VCPKG_TARGET_TRIPLET="%OneOfThree%-windows-static"


:: windows-env.bat

if ERRORLEVEL 1 exit /b 1
call shell\windows-build.bat
