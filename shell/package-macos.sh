#!/bin/bash

# $1 - Project directory (required).
# $2 - Project (required).
# $3 - Version (required).
# $4 - Distribution (unused).
# $5 - Architecture (required).

if [ "$#" -ne "5" ]; then
  if [ "$#" -lt "5" ]; then
    echo "Error: missing parameters."
  else
    echo "Error: extra parameters."
  fi

  echo "Usage: $0 [projectdir] [projectname] [projectversion] [distribution] [x86_64]"
  exit 1
fi

# Make script easier to read.
ProjDir="$1"
Project="$2"
project="$(echo "$2" | tr '[:upper:]' '[:lower:]')"
Version="$3"
Dist="$4"
Arch="$5"

if [ "$Arch" != "x86_64" ]; then
  echo "Error: invalid architecture (use 'x86_64')."
  exit 1
fi

Resources="Resources"

# Create macOS package.
rm -rf .build >/dev/null
mkdir .build
cd .build &&
cmake -DCMAKE_INSTALL_PREFIX=root/usr/local .. &&
make install &&
mkdir ${Resources} &&
pkgbuild --root root --identifier online.awise.${Project}.terminal \
    --version "${Version}" terminal.pkg &&
cp -f ../documentation/license.html ${Resources} &&
cp -f ../installation/macos/background.png ${Resources} &&
productbuild --distribution distribution.xml \
    --resources ${Resources} --version "${Version}" \
    "${project}-${Version}-darwin.x86_64.pkg" &&
cp -f "${project}-${Version}-darwin.x86_64.pkg" "${ProjDir}" &&
cd "${ProjDir}"
