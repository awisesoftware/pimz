@echo off

if (%VCPKG_ROOT_DIR%)==() (
  echo Error: VCPKG_ROOT_DIR is not set.
  exit /b 1
)
if (%VCPKG_TARGET_TRIPLET%)==() (
  echo Error: VCPKG_TARGET_TRIPLET is not set.
  exit /b 1
)
rmdir /s /q .build >nul 2>&1
mkdir .build
if ERRORLEVEL 1 exit /b 1
cd .build
if ERRORLEVEL 1 exit /b 1
cmake -DCMAKE_TOOLCHAIN_FILE=%VCPKG_ROOT_DIR%/scripts/buildsystems/vcpkg.cmake -DVCPKG_TARGET_TRIPLET=%VCPKG_TARGET_TRIPLET% -G "NMake Makefiles" ..
if ERRORLEVEL 1 exit /b 1
nmake /s /nologo
if ERRORLEVEL 1 exit /b 1
cd ..
